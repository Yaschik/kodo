def pidfile
  if Rails.env.production?
    File.join(ENV['SHARED_PATH'], 'pids', 'the_one_worker.pid')
  else
    '/tmp/kodo_workers.pid'
  end
end

def pid
  Integer(File.read(pidfile))
end

def check_pidfile!
  Process.kill(0, pid)
  abort 'Worker already running'
rescue
  File.unlink(pidfile) rescue nil
  # No pid or pidfile - good
end

namespace :workers do
  desc 'run selected worker'
  task run: :environment do
    type = ENV['TYPE'].camelize
    worker = "#{type}Worker".constantize
    worker.new.run!
  end

  desc 'start superviser'
  task :start do
    check_pidfile!
    begin
      pid = spawn 'bundle exec rake workers:run TYPE=the_one'
      File.write(pidfile, pid)
      Process.detach pid
    rescue => e
      Process.kill 'KILL', pid rescue nil
      raise e
    end
  end

  desc 'stop superviser'
  task :stop do
    begin
      Process.kill 'TERM', pid
      File.unlink(pidfile)
    rescue Errno::ENOENT
      puts 'Unable to locate pidfile'
    rescue Errno::ESRCH
      puts "No such process: #{pid}"
      File.unlink(pidfile)
    end
  end

  desc 'restart superviser'
  task :restart do
    Rake::Task['workers:stop'].invoke
    Rake::Task['workers:start'].invoke
  end
end
