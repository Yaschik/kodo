module ApplicationHelper
  def steam_log_in_button
    img_url =
        'http://cdn.steamcommunity.com/public/images/signinthroughsteam/sits_large_noborder.png'
    link_to image_tag(img_url), '/auth/steam'
  end

  def admin_root_path
    url_for [:admin, :item_classes]
  end

  def hs(code)
    "&#{code};".html_safe
  end

  delegate :role, to: :current_user
end
