PythonError = Class.new(StandardError)

class BotOrderUpdateProcessor < Que::Job
  def run(id)
    order = BotOrder::SendItems.find(id)

    if order.should_be_rolled_back?
      report(order.error)
      order.rollback! status: order.status
    end
  end

  def report(error)
    return unless error.present?
    exception = PythonError.new error
    ExceptionNotifier.notify exception
  end
end
