class Kodo::BetProcessor < Que::Job
  def run(ids)
    Lottery::Participation.where(id: ids).includes(:lottery).each do |participation|
      participation.lottery.process_bet(participation)
    end
  end
end
