Roles::Admin = Role.new do
  self.admin_scope_access = true

  def can?(*)
    Hash.new { true }
  end
end
