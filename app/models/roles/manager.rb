Roles::Manager = Role.new do
  self.admin_scope_access = true

  inherit Roles::User
  allow 'admin/item_classes'
  allow 'admin/lotteries/kodo'
  allow 'admin/lotteries/headshot'

  allow 'admin/users', user: true, disabled: true
  allow 'admin/creditings', :index
end
