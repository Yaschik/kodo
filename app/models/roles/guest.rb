Roles::Guest = Role.new do
  allow :news, :index
  allow :news, :show
  allow :sessions, %i[create show]
  allow :welcome
  allow :robokassa, :result

  allow :stones
  allow :fifties
  allow :lotteries, %i[show last wait]
  allow :lottery_participations, :stats
  allow :items, :index

  allow :stats, :show

  allow :home
end
