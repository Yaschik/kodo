Roles::User = Role.new do
  inherit Roles::Guest
  allow :sessions, %i[destroy show]
  allow :robokassa
  allow :tickets

  allow :stones
  allow :fifties
  allow :lotteries
  allow :lottery_participations
  allow :items
end
