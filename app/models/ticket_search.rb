class TicketSearch < ApplicationSearch
  equal_filter :category, default: Ticket.categories, provide: false
  equal_filter :user, User
  attribute :open, :Boolean, default: true, strict: false

  delegate :id, to: :user, prefix: true

  def user
    super || User.new
  end

  def scope
    @scope ||= Ticket.all
  end

  def filter_by_open!
    return unless open
    scope.where!(closed: false)
  end
end
