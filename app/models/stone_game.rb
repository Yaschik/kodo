class StoneGame < ActiveRecord::Base

  def self.join_game(game, user)
    game.update(user2: user, status: 'play')
    StoneRound.create(stone_game: game.id, created_at: Time.now+8)
    User.find(game.user1).increase_balance!(-game.factor)
    User.find(game.user2).increase_balance!(-game.factor)
    return game
  end


  def self.close_game(game)
    if game.status!='close'
      game.update(status: 'close')
      if game_winner(game.id)==1
        User.find(game.user1).increase_balance!(game.factor*1.8)
      elsif game_winner(game.id)==2
        User.find(game.user2).increase_balance!(game.factor*1.8)
      end
    end
  end

  def self.participate(user_id, factor)
    tmp=StoneGame.where("(status='open' OR status='play') AND "+'(user1=? OR user2=?)', user_id, user_id)
    if tmp.count>0
      return tmp.first.status
    else
      game_to_join=StoneGame.where("(status='open' OR status='play') AND "+'user1!=? and user2 is NULL and factor=?', user_id, factor).first
      if game_to_join
        StoneGame.join_game(game_to_join, user_id)
        return game_to_join
      else
        return create_game(user_id, factor)
      end
    end
  end

  def self.create_game(user, factor)
    StoneGame.create(status: 'open', user1: user, factor: factor, created_at: Time.now)
    return true
  end

  def self.bet(current_user_id, game_id, bet) #Делаем ставку
    game=StoneGame.find(game_id)
    if game.status=='play'
        rounds=StoneRound.where(stone_game: game_id)
        if rounds.last.created_at+30<Time.now
             StoneRound.close!(rounds.last)
        else
          if current_user_id==game.user1
           StoneRound.bet(1, bet, game_id)
          elsif current_user_id==game.user2
            StoneRound.bet(2, bet, game_id)
          end
        end
    end
  end

  def self.game_winner(game) #Получаем никнейм и аватарку победителя
    rounds=StoneRound.where(stone_game: game.id)
    user1_score=0
    user2_score=0
    rounds.each do |round|
      case StoneRound.calc_winner(round)
        when 1
          user1_score+=1
        when 2
          user2_score+=1
      end
    end
    if user1_score>user2_score
      return {nickname: User.find(game.user1).nickname, avatar: User.find(game.user1).avatar}
    else
      return {nickname: User.find(game.user2).nickname, avatar: User.find(game.user2).avatar}
    end
  end

  def self.get_round(game_id)
    return StoneRound.where(stone_game: game_id)
  end

  def self.check(game)
    if winner_exists?(game.id)
      close_game(game)
      return true
    else
      rounds=StoneRound.get_rounds(game.id)
      if rounds.last.created_at+30<Time.now
        if StoneRound.draw?(rounds.last)
          StoneRound.redraw!(rounds.last)
        else
          StoneRound.close!(rounds.last)
          if rounds.count<5
            if !winner_exists?(game.id)
              if StoneRound.closed?(rounds.last)
                 StoneRound.create(stone_game: game.id, created_at: Time.now)
              end
            end
          else
            StoneGame.close_game(game)
            return true
          end
        end
        return false
      else
        return true
      end
    end
  end

  def self.rounds_info(game_id)
    check(StoneGame.find(game_id))
    tmp=StoneRound.where(stone_game: game_id).order(:created_at)
    if tmp.last.user1_pass and tmp.last.user2_pass and StoneRound.closed?(tmp.last)
      if tmp.count<5
          StoneRound.create(stone_game: game_id, created_at: Time.now)
          StoneRound.write_round!(tmp.last)
      end

    end
    ActiveRecord::Base.connection.uncached do
      tmp=StoneRound.where(stone_game: game_id).order(:created_at)
    end
    result=[]
    tmp.each do |round|
      if round.user1_pass and round.user2_pass
        StoneRound.write_round!(round)
        result<<round
      else
        result<<{id: round.id, stone_game: round.stone_game, created_at: round.created_at, status: round.status}
      end
    end
    return result
  end

  def self.get_winner(game_id)
    if winner_exists?(game_id)
      if game_winner(game_id)==1
        return {nickname:User.find(StoneGame.find(game_id).user1).nickname,avatar:User.find(StoneGame.find(game_id).user1).avatar}
      elsif game_winner(game_id)==2
        return {nickname:User.find(StoneGame.find(game_id).user2).nickname,avatar:User.find(StoneGame.find(game_id).user2).avatar}
      end
    else
      return {status:'none'}
    end

  end

  def self.game_winner(game_id)
    rounds=StoneRound.where(stone_game: game_id)
    user1_score=0;
    user2_score=0;
    rounds.each do |round|
      winner=StoneRound.calc_winner(round)
      if winner==1
        user1_score+=1
      elsif winner==2
        user2_score+=1
      end
    end
    if user1_score>user2_score
      return 1
    else
      return 2
    end
  end

  def self.winner_exists?(game_id)
    rounds=StoneRound.get_rounds(game_id)
    if rounds.count>=3
      users_wins=[]
      2.times { users_wins<<0 }
      rounds.each do |round|
        winner_of_round=StoneRound.calc_winner(round)
        if winner_of_round==1
          users_wins[0]+=1
        elsif winner_of_round==2
          users_wins[1]+=1
        end
      end
      if (users_wins[0] >= 3) or (users_wins[1] >= 3)
        return true
      else
        return false
      end
    else
      return false
    end
  end


end
