class Item < ActiveRecord::Base
  belongs_to :item_class
  belongs_to :bot_order, class: BotOrder::SendItems
  delegate :name, to: :item_class
end
