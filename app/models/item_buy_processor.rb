class ItemBuyProcessor
  include Tainbox

  NoOfferLink = Class.new(StandardError)

  class NotEnoughItems < StandardError
    attr_reader :item_class

    def initialize(item_class)
      @item_class = item_class
    end
  end

  LOCK_KEY = 905_890_376_064 # SecureRandom.random_number 1_000_000_000_000

  attribute :user, User
  attribute :items, array_of(ItemClass), strict: true
  attribute :total_cost, Integer, provide: false, default: 0

  def process!
    transaction do
      connection.execute("SELECT pg_advisory_xact_lock(#{LOCK_KEY})")
      user.reload
      raise NoOfferLink unless user.steam_offer_link
      args = []
      items.each { |k, v|
        buy_item k, v
        args<<k.steam_id.to_s
      }
      args=args.join(', ').to_s
      user.increase_balance!(-total_cost)
      bot_order.update!(total_cost: total_cost,offer_link: user.steam_offer_link, role:'storage',data: args,run_at: Time.now)
      ItemClassCache.reload!
    end
  end

  def buy_item(klass, count)
    raise NotEnoughItems, klass unless klass.enabled?
    self.total_cost += klass.price * count unless attribute_provided? :total_cost
    result_count =
        klass.reload.items.limit(count).where(bot_order: nil).update_all(bot_order_id: bot_order.id)
    raise NotEnoughItems, klass if result_count < count
  end

  def items=(*)
    super
    @items = @items.group_and_count
  end

  private

  def bot_order
    @bot_order ||= BotOrder::SendItems.create!(user: user)
  end

  delegate :connection, :transaction, to: Item
end
