class FiftyHigh < ActiveRecord::Base

  def self.take_one
    FiftyHigh.last
  end

  def self.participate(high, end_time)
    transaction do
      if FiftyHigh.take_one.nil?
        FiftyHigh.create(high: high, end_time: end_time)
      else
        if FiftyHigh.take_one.high < high
          FiftyHigh.take_one.update!(high: high, end_time: end_time)
        end
        unless FiftyHigh.take_one.end_time.today?
          FiftyHigh.take_one.update!(high: high, end_time: end_time)
        end
      end
    end
  end
end