module Synchronizable
  def synchronize(*args, &block)
    lock_string = "#{self.class.name}-#{try(:id)}-#{args.inspect}"

    ResultPicker.pick do |result_picker|
      ActiveRecord::Base.transaction(requires_new: true) do
        ActiveRecord::Base.connection.execute <<-SQL
        SELECT pg_advisory_xact_lock('#{Zlib.crc32 lock_string}');
        SQL

        block.call(&result_picker)
      end
    end
  end
end
