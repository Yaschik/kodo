module TableCacher
  def scope(&block)
    if block
      @scope = block
    else
      @scope.call
    end
  end

  def all
    Rails.cache.fetch(key) { scope.all.to_a }
  end

  def reload!
    clear!
    all
  end

  def clear!
    before_reload!
    Rails.cache.delete(key)
  end

  protected

  def before_reload!; end

  private

  def version
    @version ||= timestamp
  end

  def key(*args)
    ['table_cacher', name.downcase, *args.map(&:to_s)].join('-')
  end
end
