class BotOrder::SendItems < BotOrder::Base
  has_many :items, foreign_key: :bot_order_id
  belongs_to :user

  validates :user_id, presence: true

  enum status: %w[pending accepted sent done ignored rejected error]
                   .map { |x| [x, x] }.to_h

  def rollback!(status: :rejected)
    transaction do
      items.update_all bot_order_id: nil
      user.increase_balance! total_cost
      self.status = status if status
      self.total_cost = 0
      save!
      ItemClassCache.reload!
    end
  end

  def should_be_rolled_back?
    status.in? %w[ignored rejected error]
  end
end
