class Lottery::LifetimeStat < ActiveRecord::Base
  self.table_name = :lottery_lifetime_stats

  delegate :[], :[]=, to: :data

  def self.with(x)
    raise ArgumentError unless block_given?
    stat = self.for(x) or return
    yield stat
    stat.save!
  end

  def self.for(x)
    klass = case x
            when Class then x
            when Lottery::Base then x.class
            when String then "lottery/#{x}".camelize.constantize
            else raise TypeError
            end
    find_by(lottery_type: klass.name)
  end
end
