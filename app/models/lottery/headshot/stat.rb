class Lottery::Headshot::Stat < ActiveRecord::Base
  self.table_name = :headshot_lottery_stats

  def append(participation)
    self.bets += participation.bet
    self.wins += participation.win
    self.participations_count += 1
    self.winners += participation.win.nonzero? ? 1 : 0

    grouped_win = groups[participation.win.to_s]
    grouped_win = grouped_win.to_i + 1
    groups[participation.win.to_s] = grouped_win

    save!
  end
end
