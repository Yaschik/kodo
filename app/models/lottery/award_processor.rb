class Lottery::AwardProcessor
  attr_accessor :participation, :lottery
  delegate :selection, to: :participation
  delegate :super_prize, :lifetime_stat, to: :lottery
  method_stub :award!

  def self.bets(*args)
    args.each do |name|
      define_method(:"#{name}_bets") do
        selection[name.to_s.pluralize]
      end
    end
  end

  def initialize(parent, participation)
    self.lottery = parent
    self.participation = participation
  end
end
