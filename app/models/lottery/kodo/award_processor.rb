class Lottery::Kodo::AwardProcessor < Lottery::AwardProcessor
  bets :number, :gun
  delegate :numbers, :gun, :win_table, :stat, :metadata, :ticket_cost, to: :lottery
  delegate :count, to: :gun_bets, prefix: true

  def number_hits
    @number_hits ||= (numbers & number_bets).length
  end

  def gun_hits
    @gun_hits ||= gun_bets.include?(gun) ? 1 : 0
  end

  def super_prize?
    number_hits == 8 && gun_hits == 1
  end

  def table_ratio
    win_table[number_hits].try(:[], gun_hits).try(:[], gun_bets_count).try(:nonzero?)
  end

  def ratio
    return unless table_ratio
    table_ratio.to_f / (gun_bets.length * ticket_cost)
  end

  def metadata
    {
      number_hits: number_hits,
      gun_hits: gun_hits,
    }
  end

  def award!
    super_prize.try(:give_to, participation) if super_prize?
    write_lifetime_stat
    if ratio
      participation.try :win!, ratio: ratio, super_prize: super_prize, **metadata
    else
      participation.try :loose!, metadata
    end
  end

  private

  def write_lifetime_stat
    return unless lifetime_stat
    numbers.each do |x|
      s = lifetime_stat['numbers'][x.to_s]
      s['count'] += 1
      s['last_seen'] = metadata['number']
    end
  end
end
