class Lottery::Kodo::Stat < ActiveRecord::Base
  self.table_name = :kodo_lottery_stats
  belongs_to :lottery, class_name: 'Lottery::Base'

  def self.group_by_date
    group(:date).select(<<-SQL)
      SUM(participations_count) AS participations_count,
      SUM(bets) AS bets,
      SUM(wins) AS wins,
      SUM(winners) AS winners,
      COUNT(*) AS lotteries_count,
      LAST(current_superprize) AS current_superprize,
      date
    SQL
  end

  def push(participation)
    self.participations_count += 1
    self.bets += participation.bet
    save
  end

  def cache!
    connection.execute <<-SQL
      UPDATE kodo_lottery_stats
      SET participations_count = participations.count,
          bets = CASE WHEN _bets IS NOT NULL THEN _bets ELSE 0 END,
          wins = CASE WHEN _wins IS NOT NULL THEN _wins ELSE 0 END,
          winners = CASE WHEN _winners IS NOT NULL THEN _winners ELSE 0 END,
          current_superprize = #{lottery.current_superprize}
      FROM (
        SELECT
          SUM(bet) as _bets,
          SUM(win) as _wins,
          COUNT(*) as count,
          COUNT(CASE WHEN win > 0 THEN 1 END) as _winners
        FROM lottery_participations
        WHERE lottery_participations.lottery_id = #{lottery_id}
      ) AS participations
      WHERE kodo_lottery_stats.id = #{id}
    SQL

    reload
  end
end
