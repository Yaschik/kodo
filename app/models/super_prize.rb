class SuperPrize < ActiveRecord::Base
  def self.for(x)
    klass = x.is_a?(Class) ? x : x.class
    find_by(lottery_type: klass.name)
  end

  def give_to(participation)
    winning_participations << participation
  end

  def process_bet!(participation)
    increase! participation.bet
  end

  def increase!(x)
    update! amount: amount + x * ratio
  end

  def process_win!(participation)
    self.amount -= participation.win
  end

  # @return [Boolean] true if superprize played, false otherwise
  def finalize!
    _finalize.tap { save! }
  end

  private

  def _finalize
    count = winning_participations.size
    return false if count.zero?

    saved_amount = amount
    return false if saved_amount < 0

    winning_participations.each do |participation|
      participation.win! amount: saved_amount / count
    end

    update! amount: 0
  end

  def winning_participations
    @winning_participations ||= []
  end
end
