function randomArray(min, max, sum, condition) {
    var arr = [];

    for (var i = 0; i < sum; i++) {
        var tmp = randomNumber(min, max, condition);
        var addElement = true;
        for (var j = 0; j < arr.length; j++) {
            if (arr[j] === tmp) {
                addElement = false;
                i--;
            }
        }
        if (addElement) {
            arr[i] = tmp;
        }
    }

    return arr;
}
function randomNumber(min, max, condition) {
    var numb = randomInt(min, max);
    if (condition === 'odd') {
        while ((numb & 1) != 1) {
            numb = randomInt(min, max);
        }
    } else if (condition === 'even') {
        while ((numb & 1) != 0) {
            numb = randomInt(min, max);
        }
    }
    return numb;
}
function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 0.99999) + min);
}
function oddOrEven(target) {
    return (target & 1) ? 'odd' : 'even';
}

function openAndWait(url, callback) {
    // Specified width without value to force popup open
    var popup = window.open(url, '_blank', 'width'),
        intervalLambda = function() {
            if (true == popup.closed) {
                window.clearInterval(interval);
                callback();
            }
        },
        interval = window.setInterval(intervalLambda, 50);
};

function load_data(target, data) {
    $.each(data, function(k,v) { target[k] = v; })
}

function num2s(number, digits) {
    var asStr = number + '',
        numLen = asStr.length,
        prefix = '';

    while(numLen < digits) {
        prefix += '0';
        numLen += 1;
    }

    return prefix + asStr;
}

function dup(object) {
    return JSON.parse(JSON.stringify(object));
}

function presence(something) {
    return something != undefined;
}
