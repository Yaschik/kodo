$(function() {
  var urlHelper = function() {
    var param_name = $('meta[name=csrf-param]').attr('content'),
        param_val = $('meta[name=csrf-token]').attr('content');

    this.csrf_param = {};
    this.csrf_param[param_name] = param_val;

    $.ajaxSetup({
      data: this.csrf_param
    });
  };

  window.urlHelper = urlHelper();
});
