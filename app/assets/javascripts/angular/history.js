app.controller('historyController', function ($http, $scope, $rootScope) {
	var history = this;
	this.filter = {};
	this.filter.winning = undefined;
	this.filter.circle = undefined;
	this.filter.view = 'full';
	this.rates = [];
	this.circles = [];
	this.guns = guns;


    // carousel
	this.shiftSize = 320;
	this.shiftNumb = 0;
	this.shift = function () {
		return this.shiftSize * this.shiftNumb;
	};
	this.next = function () {
		var length = $('.history__full').length - 3;
		if (this.shiftNumb > -(length)) {
			this.shiftNumb--;
		}
	};
	this.prev = function () {
		if (this.shiftNumb < 0) {
			this.shiftNumb++;
		}
	};

    this.prevGun = function(rate) {
        var index = rate.guns.indexOf(rate.currentGun);
        if(rate.guns[index - 1] !== undefined)
        {
            rate.currentGun = rate.guns[index - 1]
        }

    };

    this.nextGun = function(rate) {
        var index = rate.guns.indexOf(rate.currentGun);
        if(rate.guns[index + 1] !== undefined)
        {
            rate.currentGun = rate.guns[index + 1]
        }
    };

    this.getPreview = function (index) {
        return guns.fetch(index).preview;
    };

    this.getTheme = function (index) {
        return guns.fetch(index).theme;
    };
    this.getTitle = function (index) {
        return guns.fetch(index).title;
    };
    this.getSubtitle = function (index) {
        return guns.fetch(index).subtitle;
    };

	this.resetShift = function () {
		this.shiftNumb = 0;
	};
	$scope.$watch('history.filter.winning', function (newValue, oldValue) {
		history.resetShift();
	});
	$scope.$watch('history.filter.cirles', function (newValue, oldValue) {
		history.resetShift();
	});

    //paging
    history.paginationCurrentLink = 1;
    lastPaginationLink = 100;
    //TODO: fix it ^
    history.paginationLinks = [];


    this.update = function() {
        var decorate_circles = function(circles) {
            var min = Math.min.apply(null, circles),
                max = Math.max.apply(null, circles);

            if (min == max) {
                return '' + min;
            } else {
                return '' + min + ' - ' + max;
            }
        };

        history.resetShift();
        $http.get('/lotteries/kodo/participations/today')
            .success(function (data, status) {
                if (status === 200) {
                    history.rates = data.participations;
                    angular.forEach(history.rates, function (rate, key) {

                        // count cirles
                        angular.forEach(rate.circles, function (value) {
                            if (history.circles.indexOf(value) === -1) {
                                history.circles.push(value);
                            }
                        });

                        // count winning
                        rate.winning = $.grep(rate.numbers, function (n, i) {
                            return ( n.status === 'matched' )
                        }).length;

                        // change guns array to gun value
                        rate.currentGun = rate.guns[0];
                        rate.oneGun = rate.guns[1] === undefined;

                        rate.decorated_circles = decorate_circles(rate.circles);
                        rate.circles = rate.lottery_id;


                    });
                    history.circles.sort();
                    history.circles.reverse();
                    console.log(history.rates);
                } else if (status === 500) {
                    console.log(status, data);
                }
            });
    };

    this.update();
    $rootScope.$on('kodoBet', this.update)


});
