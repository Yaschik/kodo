app.controller('headshotCtrl', function($rootScope){
    var headhsot = this;
	this.prize = null;
	this.buy = function () {
		$.post('lotteries/headshot/bet')
            .success(function(data){
                $rootScope.$emit('updateUser', data['current_user']);
                $rootScope.$emit('headshotBet');
                headhsot.prize = data['win'];
            })
            .error(function(jqXHR) {
                var status = jqXHR.status;
                if(status == 401) {
                    $rootScope.$emit('updateUser');
                    // Do nothing
                } else if(status == 422) {
                    if(!jqXHR.responseJSON) {
                        return alert('Internal error!')
                    }
                    if(jqXHR.responseJSON.error == 'User::LowBalance') {
                        alert('Пополите счет!');
                    }
                } else if(status == 500) {
                    reportError(jqXHR.responseText)
                }
            })

	};

    (function(ctrl) {
        var backgrounds = [
            "<%= image_path 'ticket1.jpg' %>",
            "<%= image_path 'ticket2.jpg' %>",
            "<%= image_path 'ticket3.jpg' %>",
            "<%= image_path 'ticket4.jpg' %>",
            "<%= image_path 'ticket5.jpg' %>"
        ];

        ctrl.bg = function () {
            return backgrounds[randomInt(0, 4)];
        };
    }(this));
});
