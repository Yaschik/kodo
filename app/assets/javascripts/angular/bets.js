app.controller('betsCtrl', function ($scope, $http, $rootScope) {
    var bets = this;
    this.type = 'kodo';
    this.shown = false;
    this.kodo = [];
    this.headshot = [];
    this.stone = [];
    this.guns = guns;

    this.getPreview = function (index) {
        return guns.fetch(index).preview;
    };

    // get kodo bets
    this.update = function() {
        if(!$rootScope.user.logged_in) { return; }

        $http.get('lotteries/kodo/participations')
            .success(function (data, status) {
                bets.kodo = data.participations;

                angular.forEach(bets.kodo, function (rate) {
                    rate.numbers = $.grep(rate.numbers, function (n, i) {
                        return ( n.status === 'matched' || n.status === 'selected' )
                    });
                });
            });

        // get headshot bets
        $http.get('stones/history/')
            .success(function (data, status) {
                bets.stone = data;
            });
    };

    $scope.$on('betsToggle', function(event, args){
        bets.update();
        bets.shown = args;
    });
})
    .directive('bets', function(){
        return {
            templateUrl: assets.views['bets.html']
        };
    });
