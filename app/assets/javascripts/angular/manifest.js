/*
 *= require ./app
 *= require ./archive
 *= require ./balance
 *= require ./banner
 *= require ./bets
 *= require ./broadcast
 *= require ./callback
 *= require ./choice
 *= require ./date
 *= require ./fifty
 *= require ./headshot
 *= require ./history
 *= require ./superprize
 *= require ./lottery
 *= require ./stat/manifest.js
 *= require ./store/manifest.js
 *= require ./stone/manifest.js
 *= require ./user
 */
