var Stat = {};

app.directive('stat', function () {
    return {
        templateUrl: assets.views['stat.html']
    }
}).controller('statCtrl', function ($scope, $rootScope, $http, $filter) {
    var stat = this;

    this.oftenType = 'last';
    this.betsType = 'kodo';

    $scope.betNumbers = function(bet) {
        var result = [];
        $.each(bet.numbers, function() {
            if((this.status == 'selected') || (this.status == 'matched')) {
                result.push(this.index);
            }
        });
        return result.join(' ');
    };

    this.update = function() {
        $.get('/stats').done(stat._update)
    };

    this.getPreview = function (index) {
        return guns.fetch(index).preview;
    };

    this.getTitle = function (index) {
        return guns.fetch(index).title;
    };
    this.getSubtitle = function (index) {
        return guns.fetch(index).subtitle;
    };

    this._update = function(data) {
        stat.last_kodo = data['last_kodo'];
        stat.kodo_stats =  data['kodo_stats'];
        stat.kodo_bets =  data['kodo_bets'];
        stat.headshot_bets =  data['headshot_bets'];
        if(!stat.selectedLottery) {
            stat.selectedLottery = stat.last_kodo[0];
        }
    };

    this.last_kodo = [];
    this.selectedLottery = null;

    this.broadcastId = function() {
        if(stat.last_kodo[0]) {
            return stat.last_kodo[0].id;
        }
    };

    this.showBroadcast = function() {
        var id = stat.broadcastId();
        if(id) {
            $rootScope.$emit('showBroadcast', id, true);
        }
    };

    this.bets = function () {
        $rootScope.$broadcast('betsToggle', true);
    };


    $rootScope.$on('lotteryFinished', this.update);
    $rootScope.$on('kodoBet', this.update);
    $rootScope.$on('headshotBet', this.update);

    this.update();

});
