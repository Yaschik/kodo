var Store = {};

app.controller('storeCtrl', function ($http, $scope, $rootScope) {
    var store = this;

    store.items = [];
    store.filters = {};
    store.inventory = Store.inventoryProto();
    store.counts = {};

    store.filteredModel = {};
    store.filteredModel.selectedType = {};
    store.filteredModel.selectedQuality = {};
    store.filteredModel.input = "";

    this.getFilterModelParams = function(){
        $.get('/filterModel')
            .done(function(data) {
                this.filteredModel.types = data['weaponTypes'];
                this.filteredModel.qualities = data['weaponQualities'];
                this.filteredModel.starred = false;
            });
    };

    this.filter = function(model){
        $.get($.param(model)).done(function(data){
            Paginate(data);
        });
    };

    store.starttrakClicked = false;
    store.starClicked = false;

    this.stattrakToggle = function() {
        store.starttrakClicked = !store.starttrakClicked;
        this.filters.stattrak.tap();
    };

    this.starToggle = function() {
        store.starClicked = !store.starClicked;
        this.filters.stared.tap()
    };

    this.allToggle = function() {
        store.starClicked = false;
        store.starttrakClicked = false;
        this.filters.clear();
    };

    this.pushGun = function(gun) {
        var i = store.items.indexOf(gun);
        if(store.items[i].items_count != 0) {
            store.inventory.push(gun);
            store.items[i].items_count = store.items[i].items_count - 1;
        }
    };

    this.pullGun = function (gun) {
        if (typeof gun.id != 'undefined') {
            var f = 0;
            for(var j = 0; j < store.items.length; j++)
            {
                if(store.items[j].id == gun.id)
                {
                    f = j;
                }
            }
            var i = store.inventory.indexOf(gun);
            console.log(gun.id);
            store.inventory.drop(i);
            store.items[f].items_count = store.items[f].items_count + 1;
        }
    };

    this.buy = function() {
        if(store.inventory.size == 0) { return; }
        $.post('/items/buy', { items: store.inventory.asList() })
            .done(function(data) {
                alert('Покупка совершена');
                $rootScope.$emit('updateUser', data['current_user']);
                store.inventory.clear();
            }).fail(function(jqXHR) {
                var status = jqXHR.status;
                if( status == 401) {
                    $rootScope.emit('updateUser')
                } else if(status == 404) {
                    alert('Предметов уже нет в наличии');
                } else if(status == 422) {
                    var error = jqXHR.responseJSON.error;
                    if(error == 'ItemBuyProcessor::NoOfferLink') {
                        alert('Укажите offer link');
                    } else if(error == 'User::LowBalance') {
                        alert('Недостаточно денег на счете');
                    } else {
                        reportError(error);
                    }
                } else {
                    reportError(jqXHR.responseText);
                }
            })
    };

    this.update =  function() {
        $.get('/items')
            .done(function(data) {
                store.items = data['items'];
                store.filters = Store.pluckFilters(data);
            });
    };

    this.filteredItems = function() {
        var result = [];
        $.each(store.items, function(_index, item) {
            var matched = true;
            $.each(store.filters, function() {
                if(!this.match) { return; }
                if(!this.match(item)) { matched = false; }
            });
            if(matched) { result.push(item); }
        });
        return result;
    };

    this.updateOfferLink = function() {
        var val = $('[name=steam_offer_link]', '.store__change').val();
        $.post('/session', { _method: 'put', user: { steam_offer_link: val } })
            .done(function(data) { $rootScope.$emit('updateUser', data) })
            .fail(function(jqXHR) {
                var status = jqXHR.status;
                if(status == 401) {
                    $rootScope.$emit('updateUser');
                } else if(status == 422) {
                    alert('Неправильный Steam Offer Link');
                } else {
                    reportError(jqXHR.responseText);
                }
            });
    };

    this.update();
});
