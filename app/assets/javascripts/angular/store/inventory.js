Store.inventoryProto = function() {
    var inventory = [],
        flushLength = 12;

    inventory.clear = function() {
        inventory.length = 0;
        inventory.size = 0;
        inventory.total = 0;
        inventory.flush();
    };

    inventory.flush = function() {
        for(var i = inventory.size; i < flushLength; i++) { inventory[i] = { uid: Math.random() }; }
    };

    inventory.push = function(item) {
        inventory[inventory.size] = dup(item);
        inventory[inventory.size].uid = Math.random();
        inventory.total += +item['price'];
        inventory.size += 1
    };

    inventory.drop = function(id) {
        if(id >= inventory.size ) { return; }
        inventory.total -= inventory[id]['price'];
        inventory.splice(id, 1);
        inventory.size -= 1;
        inventory.flush();
    };

    inventory.asList = function() {
        return dup(inventory).slice(0, inventory.size)
            .map(function(item) {
                return item.id;
            })
    };

    inventory.clear();
    return inventory;
};
