app.controller('choiceController', function () {
	
	this.max = 8;
	
	this.selectedSum = function () {
		var selected = 0;
		angular.forEach(this.numbs, function (value) {
			if(value.selected){
				selected++;
			}
		});
		return selected;
	};
	
	this.isValid = function () {
		if( this.selectedSum() < this.max ){
			return false;
		}else{
			return true;
		}
	};
	
	this.toggleSelected = function (numbIndex) {
		var enable = false;
		if(this.isValid()){
			if(this.numbs[numbIndex].selected){
				enable = true;
			}
		}else{
			enable = true;
		}
		if(enable){
			this.numbs[numbIndex].selected = !this.numbs[numbIndex].selected
		}
	};
	
	this.numbs = [];
	for (var i = 0; i < 30; i++) {
		this.numbs.push({
			value: '30',
			selected: false
		});
	}
});