Stone.round = {};
Stone.round.end = false;

Stone.round.start = function () {
    Stone.round.timer = setInterval(function () {
        Stone.round.getState();
    }, 1000);
};

Stone.round.getState = function () {
    $.ajax({
        url: '/stones/get_info?game_id=' + Stone.registry['game'].id,
        type: 'GET'
    }).success(function (data) {
        if ((data.rounds != null)&&(Stone.round.end==false)) {
            var last_created_at=Math.floor((new Date(data.server_time)-new Date(data.rounds[data.rounds.length-1].created_at))/1000);
            console.log(last_created_at);
            if (last_created_at<=0){
                console.log("Отрицательность");
                if (data.rounds[data.rounds.length-1].status=='draw')
                {
                    Stone.registry['rounds'] = data.rounds;
                    Stone.round.renamer();
                    Stone.registry['rounds'][Stone.registry['rounds'].length-1].user1_pass=Stone.registry['lastbet'];
                    Stone.registry['rounds'][Stone.registry['rounds'].length-1].user2_pass=Stone.registry['lastbet'];
                    console.log(Stone.registry['rounds'][Stone.registry['rounds'].length-1].status);
                }
                else
                {
                    Stone.registry['rounds'] = data.rounds;
                    Stone.round.renamer();
                }
            }
            else {
                var round_time=0;
                console.log("Положительность");
                if ((30 - last_created_at)<=0)
                {
                    round_time=0;
                }
                else
                {
                   round_time = 30 - last_created_at;
                }
                if (data.rounds[data.rounds.length - 1].status == 'user1_wait') {
                    Stone.time[1] = null;
                    Stone.time[2] = round_time;
                }
                else if (data.rounds[data.rounds.length - 1].status == 'user2_wait') {
                    Stone.time[2] = null;
                    Stone.time[1] =round_time;
                }
                else {
                    Stone.time[1] = round_time;
                    Stone.time[2] = round_time;
                }
                Stone.registry['rounds'] = data.rounds;
                Stone.round.renamer();
            }
            if (data.status.status == null) {
                Stone.round.end = true;
                console.log("ИГРА КОНЧИЛАСЬ");
                Stone.registry['winner'] = data.status;
                Stone.round.wait();
            }
            else
            if (last_created_at>=30) {// если раунд идет больше 30 секунд
                Stone.round.wait();
                console.log("WANNA WAIT");}
            else
            if (last_created_at<-1){
                Stone.round.wait();
                console.log("WANNA WAIT");}

        }
    });
};


Stone.round.wait=function(){
    console.log("STARTED ME");
    clearInterval(Stone.round.timer);
    clearInterval(timer);
    var time=5;
    var timer=setInterval  (function(){
        console.log('my time'+time);
        if (time<1){
            if (Stone.round.end==true){
                Stone.round.end=false;
                Stone.registry['status']='done';
                clearInterval(Stone.round.timer);
            }
            else
            {
                Stone.registry['status']='play';
                Stone.round.start();
            }
            Stone.time[3]=null;
            clearInterval(timer);
        }
        else
        {
            time--;
            Stone.time[3]=time;
            Stone.registry['status']='wait';
        }
    },1000);
};

Stone.round.renamer=function(){
    tmp=Stone.registry['rounds'].slice();
    Stone.registry['result']=tmp;
    if (Stone.registry['result'][Stone.registry['result'].length-1].status=="draw")
    {
        Stone.registry['result'][Stone.registry['result'].length-1].status="ничья"
    }else
    if (Stone.registry['result'][Stone.registry['result'].length-1].status=="user1_wait")
    {
        Stone.registry['result'][Stone.registry['result'].length-1].status="???"
    }else
    if (Stone.registry['result'][Stone.registry['result'].length-1].status=="user2_wait")
    {
        Stone.registry['result'][Stone.registry['result'].length-1].status="???"
    }
    else
    if (Stone.registry['result'][Stone.registry['result'].length-1].status==null)
    {
        Stone.registry['result'][Stone.registry['result'].length-1].status="???"
    }
    if (Stone.registry['result'][Stone.registry['result'].length-1].status=="???")
        Stone.registry['result'].pop();
};