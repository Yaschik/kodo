Stone.search={};

    Stone.search.start=function(){
        Stone.users[1]={};
        Stone.users[2]={};
        clearInterval(Stone.search.startDdos);
        console.log('SEARCH REQ');
        Stone.search.startDdos=setInterval(function(){
            console.log('SEARCHING....');
            $.get('/stones/search?factor='+Stone.registry['game'].factor)
                .success(function(data) {
                    if (data != null) {
                            Stone.search.getUsers();
                            if (data.status=='found')
                            {
                                console.log('CONNECT TO PARTICIPATE');
                                Stone.registry['game']=data.game;
                                Stone.search.participate();
                                clearInterval(Stone.search.startDdos);
                            }
                            else
                            if (data.status=='exist')
                            {
                                console.log('CONNECT TO EXIST GAME');
                                Stone.registry['game']=data.game;
                                Stone.search.founded(0);
                                clearInterval(Stone.search.startDdos);
                            }
                            else
                            if (data.status=='wait')
                            {
                                Stone.registry['game']=data.game;
                            }
                        }
                    else
                    {
                        console.log('no such data');
                    }
                });
        },500);
    };

    Stone.search.getUsers = function () {
        $.ajax({
            url: '/stones/users',
            type: 'GET',
            data:{
                "game_id":Stone.registry['game'].id
            }
        }).success(function (data) {
            if (data.user1!=null){Stone.users[1] = data.user1;}else{
                Stone.users[1]=null;}
            if (data.user2!=null){Stone.users[2] = data.user2;}else{Stone.users[2]={};
                Stone.users[2]=null;}
        });
    };

    Stone.search.participate=function(){
        $.ajax({
            url:'/stones/participate',
            type: 'POST',
            data:{factor: Stone.registry['game'].factor}
        }).success(function(){

            clearInterval(Stone.search.startDdos);
            console.log("PARTICIPATED");
            Stone.search.founded(5);
        });

    };


    Stone.search.founded=function(latency){
        Stone.time[0]=latency;
        console.log("SEARCH FOUND");
        var Timer=setInterval(
            function(){
                if (Stone.time[0]>0){
                    Stone.time[0]--;
                }
                else{
                    Stone.time[0]=null;
                    Stone.round.start();
                    Stone.registry['status']='play';
                    clearInterval(Stone.search.startDdos);
                    clearInterval(Timer);
                }
            },1000
        );
    };

    Stone.search.sendLeave = function () {
        console.log('leave');
        clearInterval(Stone.search.startDdos);
        $.ajax(
            {
                url: '/stones/leave',
                type: 'DELETE'
            });
        Stone.registry['status'] = 'close';
    };
