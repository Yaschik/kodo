class Element
  alias_native :datetimepicker
end

module DateTimePicker
  on_redraw do |context = nil|
    context ||= Element['body']
    process = proc do |e|
      element = Element[e]
      element.datetimepicker
      element.remove_class('datetimepicker')
    end

    context.find('.datetimepicker input').each(&process)
    context.find('input.datetimepicker').each(&process)
  end
end
