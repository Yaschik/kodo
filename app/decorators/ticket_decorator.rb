class TicketDecorator < ApplicationDecorator
  decorates_association :user

  def self.categories
    HashWithIndifferentAccess.new 'issues' => 'Ошибка',
                                  'features' => 'Улучшение',
                                  'else' => 'Прочее'
  end

  def category
    self.class.categories.fetch(model.category)
  end

  def created_at
    model.created_at.in_time_zone('Moscow')
  end
end
