class ItemDecorator < ApplicationDecorator
  DICTIONARY = {
    pending: 'Заказана',
    accepted: 'Отправляется прямо сейчас',
    sent: 'Отправлена пользователю',
    done: 'Получена пользователем',
    error: 'Ошибка выдачи',
    ignored: 'Пользователь проигнорировал запрос',
    rejected: 'Пользователь отклонил запрос',
  }.stringify_keys

  def item_class
    model.item_class.market_name
  end

  def order_status
    DICTIONARY.fetch bot_order.status
  end
end
