class Lottery::Kodo::ParticipationDecorator < ApplicationDecorator
  Empty = Class.new(StandardError)
  decorates_association :lottery

  # [selected, winning]
  STATUSES = {
    [false, false] => 'empty',
    [false, true] => 'winning',
    [true, false] => 'selected',
    [true, true] => 'matched',
  }

  def as_json(*)
    super(only: %i[id user_id]).merge(
      factor: (bet / lottery.ticket_cost).to_i,
      guns: selection['guns'].try(:map, &:to_i),
      numbers: generate_numbers,
      win: !!win.try(:nonzero?),
      circles: bet_metadata['circles'],
      created_at: model.created_at.in_time_zone('Moscow'),
      lottery_id: lottery.id,
    )
  end

  private

  def numbers
    @numbers ||= selection['numbers'].map(&:to_i)
  end

  def winning_numbers
    @winning_numbers ||= begin
      result = lottery.model.result
      result ? result['numbers'].map(&:to_i) : []
    end
  end

  def generate_numbers
    return unless selection['numbers']
    (1..30).map do |i|
      selected = i.in? numbers
      winning = i.in? winning_numbers
      {
        index: i,
        status: STATUSES[[selected, winning]],
      }
    end
  end
end
