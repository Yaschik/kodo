class Lottery::ParticipationDecorator < ApplicationDecorator
  decorates_association :lottery

  DICTIONARY = {
    numbers: 'Числа',
    gun: 'Картинка',
    guns: 'Картинки',
  }

  ORDERING = DICTIONARY.keys

  def date
    h.l _date
  end

  def lottery_id
    text = "#{lottery_type}: #{lottery.id}"
    url = case lottery_type
          when 'Kodo'
            [:admin, :lotteries, :kodo, id: lottery.id]
          when 'Headshot'
            [:by_date, :admin, :lotteries, :headshot, :index, date: _date]
          end
    h.link_to text, url
  end

  def selection
    hash_to_list model.selection
  end

  def lottery_result
    hash_to_list lottery.result
  end

  def lottery_type
    lottery.type.demodulize
  end

  def matches
    return h.hs('mdash') unless lottery.is_a? Lottery::Kodo
    return h.hs('mdash') unless lottery.finished?
    result = model.selection['numbers'].map(&:to_i) & lottery.result['numbers'].map(&:to_i)
    result = result.join(', ')
    result << '<br>Картинка совпала' if model.selection['guns'].include? lottery.result['gun']
    result.html_safe
  end

  private

  def hash_to_list(hash)
    return unless hash
    hash = hash.sort_by { |key, _value| ORDERING.find_index(key.to_sym) || -1 }
    items = hash.each_with_object('') do |(key, value), object|
      object << h.content_tag(:li, "#{DICTIONARY[key.to_sym]}: #{value}")
    end
    h.content_tag :ul, items.html_safe
  end

  def _date
    @date ||= created_at.to_date
  end
end
