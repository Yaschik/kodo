class LotteryWorker < ApplicationWorker
  attr_accessor :lottery_klass

  def delay
    10.seconds
  end

  def initialize
    super
    self.lottery_klass = ENV['LOTTERY'].constantize
  end

  def logger_name
    lottery_klass.name.demodulize.downcase
  end

  def run
    create_lottery_if_needed
    finish_ended_lotteries
  end

  def create_lottery_if_needed
    return unless lottery_klass.require_next?
    lottery = lottery_klass.order(time_till: :desc).first.try(:create_next!)
    lottery_klass.create! time_from: Time.zone.now unless lottery
    updated!
  end

  def finish_ended_lotteries
    lottery_klass.past.unfinished.find_each(&:process_end!)
  end
end
