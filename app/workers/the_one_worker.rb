# The One Worker to rule them all (Daemons superviser)
class TheOneWorker < ApplicationWorker
  def delay
    1.second
  end

  def schema
    @schema ||= YAML.load(File.read(Rails.root.join('config/daemons.yml'))).tap do |schema|
      schema.each { |code, data| schema[code] = data.symbolize_keys }
    end
  end

  def pids
    @pids ||= Hash.new { |hash, key| hash[key] = [] }
  end

  def start_worker(code)
    command = schema[code][:command]
    Rails.logger.info "starting #{code} with #{command}"

    pids[code] << spawn(command).tap { |pid| Process.detach pid }
  end

  def run
    schema.each do |code, data|
      pids[code].select! { |x| status x }
      Rails.logger.debug "pids monitoring: #{pids}" if ENV['DEBUG']
      (data[:count] - pids[code].size).times do
        start_worker(code)
      end
    end
  end

  def stop
    puts 'Stopping workers superviser'
    stop_all('TERM')
    sleep 1
    stop_all('KILL')
    puts 'Stopped workers superviser'
  end

  private

  def stop_all(method)
    pids.values.each do |worker_pids|
      worker_pids.each { |pid| status(pid) and Process.kill(method, pid) }
    end
  end

  def status(pid)
    Process.kill(0, pid)
  rescue Errno::ESRCH
    # Process not found
  end
end
