module MultiForm
  class MultiFormHandler
    attr_accessor :scope, :form_klass
    delegate :map, :each, :select, :reject, :find, to: :scope
    delegate :after_submit, to: :form_klass

    def initialize(scope, form_klass)
      self.form_klass = form_klass
      self.scope = scope.map { |x| form_klass.new(x) }
    end

    def update_attributes(params)
      params.each do |key, value|
        element = find { |x| x.id == Integer(key) }
        if element
          element.update_attributes(value)
        else
          raise ActiveRecord::RecordNotFound,
                "#{scope.first.class.object_class} with id=#{key} not found"
        end
      end
    end

    def valid?
      select(&:invalid?).empty?
    end

    def submit(params)
      update_attributes params
      return unless valid?

      scope.first.class.object_class.transaction do
        each(&:save!)
        after_submit
      end

      true
    end
  end

  def after_submit(*); end

  def default_scope(&block)
    if block
      @default_scope = block
    else
      @default_scope.call
    end
  end

  def for_scope(scope = default_scope)
    MultiFormHandler.new(scope, self)
  end
  alias_method :multi_form, :for_scope
end
