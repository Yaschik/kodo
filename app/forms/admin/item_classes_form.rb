class Admin::ItemClassesForm < ApplicationForm
  extend MultiForm
  decorates :item_class
  default_scope { ItemClass.order(:id) }

  permit :price, :enabled, :best
  permit :id, :name, :rareness, :items_count, write_access: false

  def self.after_submit(*)
    ItemClassCache.reload!
  end
end
