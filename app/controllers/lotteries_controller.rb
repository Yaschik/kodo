class LotteriesController < ApplicationController
  before_filter :find_lottery

  rescue_from(Lottery::LotteryNotAvailable) { render_404 }

  def bet
    raise Lottery::LotteryNotAvailable unless @lottery
    participation = @lottery.participate! current_user, params
    render json: { win: participation.win, current_user: current_user }
  rescue User::LowBalance
    render json: { error: 'User::LowBalance' }, status: :unprocessable_entity
  end

  def multi_bet
    processor = @lottery_klass::MultiBetProcessor.new user: current_user,
                                                      # implicit permit
                                                      bets: params[:bets],
                                                      **params.permit(:factor, :circles, :guns).symbolize_keys
    processor.process!
    render json: { current_user: current_user }
  rescue User::LowBalance
    render json: { error: 'User::LowBalance' }, status: :unprocessable_entity
  end

  def show
    render_404 unless @lottery
    render json: {
      lotteries: [@lottery.decorate], current_user: current_user,
      current_time: Time.zone.now.in_time_zone('Moscow')
    }
  end

  def wait
    number = params.require :number
    lottery = Lottery::Kodo.where.not(result: nil).find_by!("metadata ->> 'number' = ?", number)
    render json: {
      lottery: lottery.decorate,
      current_user: current_user,
    }
  end

  def last
    count = params[:count] || 1
    offset = params[:offset] || 0
    scope = @lottery_klass.finished.order(time_till: :desc)
            .filter_by_numbers_range(count: count, offset: offset)
    render json: { lotteries: scope.decorate, current_user: current_user }
  end

  private

  def find_lottery
    @type = params['id']
    render_404 unless @type.in? %w[kodo headshot]
    @lottery_klass = "Lottery::#{@type.camelize}".constantize

    @lottery = @lottery_klass.running
  end
end
