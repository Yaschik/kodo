class StonesController < ApplicationController

  def winner
    render json:StoneGame.get_winner(params[:game_id])
  end

  def show
    case params[:object]
      when 'current'
        tempgame=StoneGame.where('(user1=? or user2=?)', current_user.id, current_user.id).last
      else
        tempgame='empty'
    end
    render json: {game:tempgame}
  end

  def participate
    newgame=StoneGame.participate(current_user.id,params[:factor].to_i)
    render json:{game:newgame}
  end


  def get_info
    render json:{rounds:StoneGame.rounds_info(params[:game_id].to_i),status:StoneGame.get_winner(params[:game_id]),server_time:Time.now}
  end

  def users
    fromGame=StoneGame.find(params[:game_id].to_i)
    if fromGame
      result={}
      if fromGame.user1
        user1=User.find(fromGame.user1)
        result[:user1]={nickname:user1.nickname,avatar:user1.avatar}
      end
      if fromGame.user2
        user2=User.find(fromGame.user2)
        result[:user2]={nickname:user2.nickname,avatar:user2.avatar}
      end
        render json:result
    else
      render json:{status:'error'}
    end
  end

  def search
    tempgame=StoneGame.where('user1=? or user2=?',current_user.id,current_user.id).last
      if tempgame.status=='play' and tempgame.updated_at+5>Time.now
        render json:{status:'found',game:tempgame}
      elsif tempgame.status=='play'
          render json:{status:'exist',game:tempgame}
      elsif tempgame.status=='open'
        render json:{status:'wait',game:tempgame}
      else
        render json:{status:error}
      end
  end

  def bet
    StoneGame.bet(current_user.id, params[:game_id], params[:bet])
    render json:{status:'succes'}
  end


  def leave
    tempgame = StoneGame.where('user1=? and status=?', current_user.id, 'open').last
    if tempgame
      tempgame.update(status: 'close')
    end
    render json: {game: 'close'}
  end

  def history
    tempgames=StoneGame.where('user1=? or user2=?',current_user.id,current_user.id).order(:id).last(20)
    history=[]
    tempgames.each do |temp|
      game={}
      game[:id]=temp.id
      if temp.user1
        game[:user1]=User.find(temp.user1).nickname
      end
      if temp.user2
        game[:user2]=User.find(temp.user2).nickname
      end
      game[:factor]=temp.factor
      tmp=StoneRound.game_winner(temp)
      if tmp
        game[:winner]=User.find(tmp).nickname
      end
      game[:updated_at]=temp.updated_at

      history<<game
    end
    render json: history
  end



end