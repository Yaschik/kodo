class FiftiesController < ApplicationController


  def create

    if current_user.fifties.active.nil?
      factor = params[:factor]
      Fifty.participate(current_user, Integer(factor))
      render json: {current_user: @current_user}
    else
      Fifty.take_bet(current_user)
      render json: {current_user: @current_user}
    end
  end

  def bet
    if current_user.fifties.active.nil?
      render json: {current_user: @current_user}
    else
      chosen = params[:chosen] || 0
      iter = current_user.fifties.active.id
      Fifty.make_bet(current_user, Integer(chosen))
      render json: {current_user: current_user, active: current_user.fifties.find_id(iter)}
    end

  end

  def show
    #  @current_user.fifties.active.delete
    render json: {active: current_user.fifties.active, current_time: Time.now}
  end

  private


end