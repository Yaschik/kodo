class ItemsController < ApplicationController
  def buy
    ItemBuyProcessor.new(buy_params).process!
  rescue ItemBuyProcessor::NotEnoughItems
    render_404
  rescue ItemBuyProcessor::NoOfferLink
    render json: { error: 'ItemBuyProcessor::NoOfferLink' }, status: :unprocessable_entity
  rescue User::LowBalance
    render json: { error: 'User::LowBalance' }, status: :unprocessable_entity
  else
    render json: { current_user: current_user }
  end

  def index
    render json: { **ItemClassCache.as_json, current_user: current_user }
  end

  private

  def buy_params
    if params[:user] && can?(:grant)
      params.permit :user, :total_cost, :items
    else
      {
        items: params[:items],
        user: current_user,
      }
    end
  end
end
