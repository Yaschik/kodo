class SessionsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: :create

  def create
    user_login
  rescue UserSession::LoginFailed
    head :unprocessable_entity
  else
    render 'application/none', layout: false
  end

  def update
    attrs = params.require(:user).permit(:steam_offer_link, :sound)

    current_user.update_attributes(attrs)

    if current_user.save
      render json: { current_user: current_user }
    else
      head :unprocessable_entity
    end
  end

  def destroy
    log_out!
    render json: { current_user: current_user }
  end

  def show
    render json: { current_user: current_user }
  end
end
