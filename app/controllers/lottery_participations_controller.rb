class LotteryParticipationsController < ApplicationController
  def index
    render_from_search \
      Lottery::ParticipationSearch.new(lottery_types: [params[:id]], user: current_user,
                                       **params.permit(:count, :offset))
  end

  def today
    render_from_search \
      Lottery::ParticipationSearch.new(
        lottery_types: [params[:id]],
        user: current_user,
        date: Date.today,
      )
  end

  # Deprecation warning: count=all is deprecated, use empty count instead
  def stats
    params[:count] ||= 'all'
    if params[:count] == 'all'
      numbers =  all_stats_numbers
    else
      numbers = lottery_klass.finished.order(time_till: :desc).limit(params[:count]).numbers_stats
    end

    render json: { numbers: numbers, current_user: current_user }
  end

  private

  def all_stats_numbers
    Lottery::LifetimeStat.for(params[:id])['numbers']
  end

  def lottery_klass
    @lottery_klass = "lottery/#{params[:id]}".camelize.constantize
  end

  def render_from_search(search)
    scope = lottery_klass::ParticipationDecorator.decorate_collection(search.scope.order(:id).last(20))
    render json: {
      current_user: current_user,
      participations: scope,
    }
  end
end
