class Admin::CreditingsController < Admin::AdminController
  before_action :find_user

  def index
    @creditings = @user.creditings
  end

  def create
    creation_params = params.permit(:amount)
    creation_params[:source] = "Начисление из админки юзером ID=#{current_user.id}"

    @user.creditings.create! creation_params
    redirect_to [:admin, :creditings]
  rescue User::LowBalance
    redirect_to [:admin, :creditings], error: 'Нельзя снять больше, чем есть'
  end

  private

  def find_user
    @user = User.find(params[:id])
  end
end
