class Admin::Lotteries::HeadshotController < Admin::AdminController
  def index
    @stats = Lottery::StatDecorator.decorate_collection \
      Lottery::Headshot::Stat.order(date: :desc)
  end

  def by_date
    @date = params[:date].to_date rescue render_404
    @stats = Lottery::ParticipationSearch.new(date: @date, lottery_types: %w[Headshot])
             .grouped_for_headshot_stats
  end
end
