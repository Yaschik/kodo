class Admin::Lotteries::StoneController < Admin::AdminController

  def index
    @games=StoneGame.all.order(:id).paginate(:page=>params[:page])
  end

  def close
    tmp=StoneGame.find(params[:id])
    tmp.update(status:'close')
    redirect_to [:admin, :lotteries,:stone,:index]
  end

  def show
    @game=StoneGame.find(params[:id])
    @rounds=StoneRound.where(stone_game:params[:id])
  end

end
