class Admin::UsersController < Admin::AdminController
  before_action :find_user, except: :index

  def index
    @search = UserSearch.new(params[:search])
    @users = @search.scope
  end

  def show
    @form = Admin::UserForm.new @user
  end

  def update
    @form = Admin::UserForm.new @user
    if @form.submit(params.require(:user))
      redirect_to [:admin, @user]
    else
      render :show
    end
  rescue ApplicationForm::AccessError
    redirect_to [:admin, @user], error: 'Ошибка доступа к пользователю'
  end

  def participations
    params[:search] ||= {}
    @search = Lottery::ParticipationSearch.new(params[:search].merge(user: @user))
    @stats = @search.scope.order(updated_at: :desc).includes(:lottery).decorate
  end

  def items
    @items = @user.items.includes(:bot_order).decorate
  end

  private

  def find_user
    @user = User.find(params[:id])
  end
end
