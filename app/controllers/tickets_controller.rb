class TicketsController < ApplicationController
  before_action :find_ticket, except: %i[index new create]
  #
  # def index
  #   @tickets = current_user.tickets.order(created_at: :desc)
  # end

  def new
    @ticket = Ticket.new
  end

  def create
    @ticket = Ticket.new(user: current_user)
    @ticket.update_attributes(
      params.require(:ticket)
          .permit(:title, :category, :body, :user_name, :user_email),
    )
    if @ticket.save
      render json: { current_user: current_user }
    else
      render json: { errors: { ticket: @ticket.errors } }, status: :unprocessable_entity
    end
  end
  
  def show_tickets
    @tickets_paged = Ticket.all.paginate(page:params[:page], per_page: 5)
  end

  # def reply
  #   Ticket.transaction do
  #     @ticket.tap(&:open!).save!
  #     @comment = @ticket.comments.new(user: current_user)
  #     @comment.update_attributes params.require(:ticket_comment).permit(:body)
  #     if @comment.save
  #       redirect_to @ticket
  #     else
  #       render :show
  #       raise ActiveRecord::Rollback
  #     end
  #   end
  # # end
  #
  # def close
  #   @ticket.tap(&:close!).save!
  #   redirect_to :tickets
  # end
  #
  # def show
  #   @ticket = @ticket.decorate
  #   @comment = @ticket.comments.new
  # end

  private

  def find_ticket
    @ticket = current_user.tickets.find(params[:id])
  end
end
