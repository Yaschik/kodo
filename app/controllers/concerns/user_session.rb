module UserSession
  LoginFailed = Class.new(StandardError)
  Unauthorized = Class.new(StandardError)
  extend ActiveSupport::Concern

  included do
    rescue_from(Unauthorized) { head :unauthorized }
    before_action :authorize!

    helper_method :current_user
    helper_method :can?
  end

  protected

  def user_login
    auth = request.env['omniauth.auth']
    user = User.find_by(steam_id: auth.uid)
    user ||= User.new(steam_id: auth.uid)
    user.nickname = auth.info.nickname
    user.avatar = auth.info.image
    user.save!
    log_in! user
  end

  def log_out!
    session.delete :user_id
    @current_user = User.new.decorate
  end

  def current_user
    @current_user ||= begin
      raw_user = session[:user_id] ? User.find(session[:user_id]) : User.new
      raw_user.decorate
    end
  rescue ActiveRecord::RecordNotFound
    log_out!
  end

  def authorize!
    @access = current_user.role.access(self) or raise(Unauthorized)
  end

  def can?(something)
    @access[something.to_sym]
  end

  private

  def log_in!(user)
    raise unless user.persisted?
    session[:user_id] = user.id
    @current_user = user.decorate
  end
end
