Документация KODO:

* Документация по верстке, составленная 100 лет назад, актуальность не поддерживается: [assets.md](docs/assets.md)
* Инструкция по внешней начинке сайта: [reference.md](docs/reference.md)
* Неактуальное API контроллеров: [specs.md](docs/specs.md)
* Добавлен файл [TODO.md](docs/todo.md)