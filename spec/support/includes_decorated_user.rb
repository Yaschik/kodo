shared_examples 'includes decorated user' do
  it 'includes decorated user' do
    user.reload rescue nil
    expect(response_json[:current_user].symbolize_keys)
      .to match id: user.id,
                nickname: user.nickname,
                steam_id: user.steam_id,
                admin_scope_access: user.role.admin_scope_access?,
                balance: user.balance.to_s,
                created_at: user.created_at.in_time_zone('Moscow').as_json,
                steam_offer_link: user.steam_offer_link,
                kodo_tickets: user.kodo_tickets
  end
end
