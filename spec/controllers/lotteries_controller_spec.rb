describe LotteriesController do
  describe 'POST multi_bet' do
    def subject
      post :multi_bet,
           id: 'kodo',
           bets: [{
             numbers: numbers,
             guns: guns,
           }],
           factor: factor,
           circles: circles
    end

    let(:numbers) { (1..8).to_a }
    let(:guns) { (1..4).to_a }
    let(:factor) { 1 }
    let(:circles) { 1 }

    specify 'when user not logged in' do
      expect { subject }.to raise_error(UserSession::Unauthorized)
    end

    context 'when user logged in' do
      let(:user) { create :user, balance: 100_000 }
      before { log_in user }

      it_behaves_like 'includes decorated user' do
        before { create :kodo_lottery }
        before { subject }
      end

      context 'user described bets' do
        let!(:lottery) { create :kodo_lottery }
        it { is_expected.to be_ok }

        it 'changes user balance' do
          expect { subject }.to change { user.reload.balance }.by(-120)
        end

        it 'creates particiaption' do
          expect { subject }.to change(Lottery::Participation, :count).by(1)
          expect(Lottery::Participation.last.bet_metadata).to include 'circles' => [lottery.id]
        end
      end

      context 'user used auto bets' do
        let(:circles) { 2 }

        def subject
          post :multi_bet,
               id: 'kodo',
               bets: 2,
               factor: factor,
               circles: circles,
               guns: 3
        end

        let!(:lotteries) { create_list :kodo_lottery, 2 }

        it { is_expected.to be_ok }

        it 'changes user balance' do
          expect { subject }.to change { user.reload.balance }.by(-360)
        end

        it 'creates particiaptions' do
          expect { subject }.to change(Lottery::Participation, :count).by(4)
          Lottery::Participation.last(4).each do |par|
            expect(par.bet_metadata).to include('circles' => match_array(lotteries.map(&:id)))
          end
        end
      end

      specify 'when lottery does not exist' do
        expect { subject }.to raise_error(Lottery::LotteryNotAvailable)
      end

      context 'when user balance is low' do
        before { create :kodo_lottery }
        before { subject }
        let(:user) { create :user, balance: 20 }
        include_examples 'it renders User::LowBalance error'
      end
    end
  end

  describe 'POST bet' do
    def subject
      post :bet, id: 'headshot'
    end

    specify 'when user not logged in' do
      expect { subject }.to raise_error(UserSession::Unauthorized)
    end

    context 'when user logged in' do
      let(:user) { create :user, balance: 100_000 }
      before { log_in user }

      it_behaves_like 'includes decorated user' do
        before { create :headshot_lottery }
        before { subject }
      end

      context 'when everything is ok' do
        let!(:lottery) { create :headshot_lottery, :empty }
        before { lottery.participations.create bet: 20, win: 40 }

        it 'renders good response' do
          subject
          expect(response).to be_ok
          expect(response_json[:win].to_i).to eq 40
        end

        it 'changes user balance' do
          expect { subject }.to change { user.reload.balance }.by(20)
        end

        it 'assigns participation to user' do
          expect { subject }.to change { user.lottery_participations.count }.by(1)
        end
      end

      specify 'when lottery does not exist' do
        expect { subject }.to raise_error(Lottery::LotteryNotAvailable)
      end

      context 'when user balance is low' do
        before { create :headshot_lottery }
        let(:user) { create :user, balance: 19 }
        before { subject }
        include_examples 'it renders User::LowBalance error'
      end
    end
  end

  shared_examples 'renders lotteries' do
    include_examples 'includes decorated user' do
      let(:user) { User.new }
    end

    it 'renders lotteries' do
      expect(response).to be_ok
      expect(response_json[:lotteries].first.symbolize_keys)
        .to include id: lottery.id,
                    time_from: lottery.time_from.in_time_zone('Moscow').as_json,
                    time_till: lottery.time_till.in_time_zone('Moscow').as_json,
                    result: result
    end
  end

  describe 'GET show' do
    let!(:lottery) { create :kodo_lottery }
    before { get :show, id: 'kodo' }

    include_examples 'renders lotteries' do
      let(:result) { be_nil }
    end
  end

  describe 'GET last' do
    before { create :super_prize, :kodo, amount: 100 }

    let!(:lottery) do
      create(:kodo_lottery, time_from: 1.day.ago, time_till: 1.second.ago).tap(&:process_end!)
    end
    before { get :last, id: 'kodo' }

    it_behaves_like 'renders lotteries' do
      let(:result) { match(lottery.result) }
    end

    let(:result) { response_json[:lotteries].first.symbolize_keys }

    it 'includes superprize' do
      expect(result[:current_superprize]).to eq '100.0'
      expect(result[:superprize_played]).to be_falsey
    end

    context 'superprize won' do
      let(:user) { create :user }

      let!(:lottery) do
        lottery = create :kodo_lottery, time_from: 1.day.ago, time_till: 1.second.ago
        create :lottery_participation, user: user, lottery: lottery, bet: 30,
                                       selection: { guns: [1], numbers: (1..8).to_a }

        allow(lottery).to receive(:generate_result!) do
          lottery.gun = 1
          lottery.numbers = (1..8).to_a
        end

        lottery.tap(&:process_end!)
      end

      it 'includes superprize' do
        expect(result[:current_superprize]).to eq '130.0'
        expect(result[:superprize_played]).to be_truthy
      end
    end
  end
end
