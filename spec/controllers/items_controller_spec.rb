describe ItemsController do
  let(:user) { create :user }

  describe 'POST buy' do
    def subject
      post :buy, offer_link: 'http://offer.link',
                 items: items
    end

    let(:items) { [] }

    specify 'when user not logged in' do
      expect { subject }.to raise_error(UserSession::Unauthorized)
    end

    context 'when user logged in' do
      let(:user) { create :user, balance: 1_000 }
      before { log_in user }
      let(:item_class_1) { create :item_class, price: 50 }
      let(:item_class_2) { create :item_class, price: 100 }

      before do
        5.times { create :item, item_class: item_class_1 }
        create(:item, item_class: item_class_2)
      end

      # item_class_1.id
      let(:ici1) { item_class_1.id }
      # item_class_2.id
      let(:ici2) { item_class_2.id }
      let(:unused_item_class_id) { create(:item_class).tap(&:destroy).id }

      it_behaves_like 'includes decorated user' do
        before { subject }
      end

      context 'everything is OK' do
        let(:user) { create :user, balance: 1_000 }
        let(:items) { [ici1, ici1, ici2] }
        before { subject }

        it 'returns ok' do
          expect(response).to be_ok
        end

        it 'items are taken away' do
          # buying items second time
          expect { subject }.to raise_error ActionController::RoutingError
        end
      end

      context 'not enough items' do
        let(:items) { [ici2, ici2] }
        it 'renders 404' do
          expect { subject }.to raise_error ActionController::RoutingError
        end
      end

      context 'user balance is low' do
        let(:items) { [ici1, ici1] }
        let(:user) { create :user, balance: 99 }
        before { subject }

        include_examples 'it renders User::LowBalance error'
      end
    end
  end

  describe '#index' do
    def get!
      ItemClassCache.clear!
      get :index
    end

    def populate_classes!
      ItemClass.all.each { |x| x.items.create! steam_id: generate(:uid) }
    end

    describe 'scoping' do
      before { create_list :item_class, 2, enabled: true, price: 100 }
      before { create :item_class, enabled: false, price: 100 }
      before { populate_classes! }

      it 'renders only enabled items' do
        expect(get!).to be_ok
        expect(response_json[:items].length).to eq 2
      end
    end

    describe 'decoration' do
      subject { response_json[:items].first.symbolize_keys }
      let!(:item_class) { create :item_class, enabled: true, price: 100, weapon_type: 'Нож' }
      before { populate_classes! }
      before { get! }

      it 'is ok' do
        is_expected.to match(
          id: item_class.id,
          preview: match(%r{\Ahttp://}),
          name: item_class.name,
          price: item_class.price.to_s,
          rareness: item_class.rareness,
          weapon_type: item_class.weapon_type,
          classfield: item_class.classfield,
          stared: true,
          stattrak: false,
        )
      end
    end

    specify 'grouping' do
      create :item_class, enabled: true, price: 100
      create :item_class, enabled: true, best: true, price: 100
      populate_classes!
      get!
      expect(response_json[:items].size).to eq 1
      expect(response_json[:best].size).to eq 1
    end

    specify 'rareness and other stuff' do
      create :item_class, enabled: true, price: 100, rareness: 'a', weapon_type: 'a'
      create :item_class, enabled: true, price: 100, rareness: 'b', weapon_type: 'b'
      create :item_class, enabled: true, price: 100, rareness: 'b', weapon_type: 'c'
      populate_classes!
      get!
      expect(response_json[:rarenesses]).to match_array %w[a b]
      expect(response_json[:weapon_types]).to match_array %w[a b c]
    end
  end
end
