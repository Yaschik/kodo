describe ApplicationController do
  # TODO: reimplement in terms of first jqXHR controller
  # describe 'redirect_to' do
  #   before { set_controller WelcomeController }
  #   before { xhr :get, :welcome }
  #
  #   it 'returns js responce' do
  #     expect(response.header['Content-Type']).to include 'text/javascript'
  #   end
  #
  #   it 'returns right command' do
  #     expect(response.body.strip).to eq(<<-JAVASCRIPT.strip)
  #       window.location = 'http://#{request.host}/en/news';
  #     JAVASCRIPT
  #   end
  # end

  specify 'render404' do
    expect { described_class.new.send(:render_404) }
      .to raise_error(ActionController::RoutingError)
  end
end
