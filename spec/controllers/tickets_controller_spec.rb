describe TicketsController do
  describe 'POST create' do
    def subject
      post :create, ticket: {
        user_name: 'Васек',
        user_email: 'vasya228@mail.ru',
        title: 'lorem ipsum',
        category: 'issues',
        body: '...',
      }
    end

    specify 'user is guest' do
      expect { subject }.to raise_error UserSession::Unauthorized
    end

    context 'user logged in' do
      let(:user) { create :user }
      before { log_in user }

      include_examples 'includes decorated user' do
        before { subject }
      end

      context 'with good params' do
        it { is_expected.to be_ok }

        it 'creates ticket' do
          expect { subject }.to change { user.tickets.count }.by(1)
        end
      end
    end
  end
end
