describe Admin::ItemClassesController do
  before { log_in(create :user, role: :manager) }
  subject { response }
  let!(:item) { create :item }

  describe 'GET index' do
    render_views
    before { create_list :item, 2, item_class: item.item_class }
    before { create_list :item, 2 }
    before { get :index }
    it { is_expected.to render_template 'admin/item_classes/index' }
  end

  describe 'POST update_all' do
    let(:id) { item.item_class.id }

    context 'with valid params' do
      before { post :update_all, item_class: { id => { price: 100, enabled: true } } }
      it { is_expected.to redirect_to [:admin, :item_classes] }
    end

    context 'with invalid params' do
      render_views
      before { post :update_all, item_class: { id => { price: '', enabled: true } } }
      it { is_expected.to render_template 'admin/item_classes/index' }
    end
  end
end
