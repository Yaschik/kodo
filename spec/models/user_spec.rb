describe User do
  describe '#increase_balance' do
    let(:user) { create :user, balance: 0 }

    def subject
      user.balance
    end

    context 'user balance increased to 100' do
      before { user.increase_balance! 100 }
      it { is_expected.to eq(100) }

      context 'and then decreased by 50' do
        before { user.increase_balance!(-50) }
        it { is_expected.to eq(50) }
      end

      specify 'and then decreased by 150' do
        expect { user.increase_balance!(-150) }
          .to raise_error(User::LowBalance)
      end
    end
  end
end
