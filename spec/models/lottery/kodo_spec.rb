describe Lottery::Kodo do
  before { create :super_prize, :kodo }
  let!(:lottery) { create :kodo_lottery, result: {} }

  describe Lottery::Base do
    specify '#process_end!' do
      expect(lottery).to receive :generate_result!
      expect(lottery).to receive :award_participations!
      expect(lottery).to receive :save!
      expect(lottery.super_prize).to receive :finalize!
      lottery.process_end!
    end

    specify '#award_participations!' do
      participations = 3.times.map { create :lottery_participation, lottery: lottery }
      participations.last.loose!

      result_stub = double award!: nil

      expect(Lottery::Kodo::AwardProcessor)
        .to receive(:new).with(lottery, eql(participations[0])).and_return result_stub
      expect(Lottery::Kodo::AwardProcessor)
        .to receive(:new).with(lottery, eql(participations[1])).and_return result_stub
      expect(Lottery::Kodo::AwardProcessor)
        .not_to receive(:new).with(lottery, eql(participations[2]))

      lottery.send :award_participations!
    end

    describe '#create_next' do
      it 'creates new lottery of the same type' do
        expect { lottery.create_next! }.to change(Lottery::Kodo, :count).by(1)
        expect { lottery.create_next! }.to change(Lottery::Base, :count).by(1)
      end

      it 'sets time and drops result' do
        lottery.create_next!
        new_lottery = Lottery::Kodo.last
        expect(new_lottery.time_from).to be_within(0.001.second).of lottery.time_till
        expect(new_lottery.time_till)
          .to be_within(0.001.second).of(new_lottery.time_from + lottery.send(:duration))
        expect(new_lottery.result).to be_nil
      end
    end
  end

  describe '#award_participations' do
    let!(:participation) do
      create :lottery_participation,
             selection: selection,
             lottery: lottery,
             bet: 30,
             user: user
    end

    let(:selection) { { guns: guns, numbers: numbers } }
    let(:user) { create :user, balance: 0 }
    let(:super_prize) { lottery.super_prize }

    def subject
      user.reload.balance
    end

    before do
      super_prize

      expect(lottery).to receive(:generate_result!) do
        lottery.numbers = (1..8).to_a
        lottery.gun = 1
      end

      lottery.process_end!
    end

    context 'user wins something' do
      let(:guns) { [2] }
      let(:numbers) { (1..8).to_a }

      it { is_expected.to eq 20_000 }

      specify 'stats updated' do
        expect(lottery.stat.attributes.symbolize_keys)
          .to include(
            participations_count: 1,
            wins: 20_000,
            bets: 30,
            winners: 1,
          )
      end
    end

    context 'user wins nothing' do
      let(:guns) { [1] }
      let(:numbers) { (9..16).to_a }

      it { is_expected.to eq 0 }

      specify 'stats updated' do
        expect(lottery.stat.attributes.symbolize_keys)
          .to include(
            participations_count: 1,
            wins: 0,
            bets: 30,
            winners: 0,
          )
      end
    end

    context 'user wins super prize' do
      let(:super_prize) do
        lottery.super_prize.tap { |x| x.update! amount: 300_000 }
      end

      let(:guns) { [1, 2] }
      let(:numbers) { (1..8).to_a }

      it { is_expected.to eq 335_000 }

      specify 'stats updated' do
        expect(lottery.stat.attributes.symbolize_keys)
          .to include(
            participations_count: 1,
            wins: 335_000,
            bets: 30,
            winners: 1,
          )
      end
    end
  end

  specify '#generate_result!' do
    lottery.send :generate_result!
    expect(lottery.numbers.count).to eq 8
    expect(lottery.gun).to be_in 1..4
  end

  describe '#participate!' do
    let(:params) { { 'bet' => 30, 'guns' => [1], 'numbers' => (1..8).to_a } }
    let(:user) { create :user, balance: 40 }

    subject { -> { lottery.participate!(user, params) } }

    context 'when everything is ok' do
      it { is_expected.to change(Lottery::Participation, :count).by 1 }
    end

    context 'when params are invalid' do
      shared_examples 'it renders error' do
        it { is_expected.not_to change(Lottery::Participation, :count) }
        it { is_expected.not_to change { user.reload.balance } }

        it 'returns lottery validator with errors' do
          result = subject.call
          expect(result.errors).to include error_field
        end
      end

      context '10 numbers' do
        let(:error_field) { :numbers }

        def params
          super.merge('numbers' => (1..10).to_a)
        end

        it_behaves_like 'it renders error'
      end

      context 'number out of range' do
        let(:error_field) { :numbers }

        def params
          super.merge('numbers' => (0..7).to_a)
        end

        it_behaves_like 'it renders error'
      end

      context '0 guns' do
        let(:error_field) { :guns }

        def params
          super.merge('guns' => [])
        end

        it_behaves_like 'it renders error'
      end

      context 'gun out of range' do
        let(:error_field) { :guns }

        def params
          super.merge('guns' => [0, 1])
        end

        it_behaves_like 'it renders error'
      end
    end

    context 'when users balance is not enough' do
      let(:user) { create :user, balance: 0 }
      it { is_expected.to raise_error User::LowBalance }
    end
  end
end
