describe Crediting do
  let(:user) { create :user }

  specify 'after create increases balance' do
    expect { create :crediting, amount: 100, user: user }
      .to change { user.reload.balance }
  end
end
