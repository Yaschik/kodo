describe SuperPrize do
  let!(:super_prize) { create :super_prize, :kodo, amount: 100, ratio: 2 }
  let(:lottery) { create :kodo_lottery }
  let(:participation) { create :lottery_participation, lottery: lottery, bet: bet, win: win }
  let(:bet) { 0 }
  let(:win) { 0 }

  subject { super_prize }

  describe '#give_to' do
    specify 'one participation' do
      subject.give_to participation
      subject.finalize!
      expect(participation.reload.win).to eq(100)
    end

    specify 'two participations' do
      participation2 = participation.dup.tap(&:save!)
      subject.give_to participation
      subject.give_to participation2
      subject.finalize!
      expect(participation.reload.win).to eq(50)
      expect(participation2.reload.win).to eq(50)
    end
  end

  describe '#process_bet!' do
    let(:bet) { 30 }

    it 'increases amount' do
      # create participation before it would be tested
      participation

      expect { subject.process_bet! participation }
        .to change { subject.amount }.by(60)
    end
  end

  describe '#process_win!' do
    let(:win) { 30 }

    it 'decreases amount' do
      expect { subject.process_win! participation }
        .to change { subject.amount }.by(-30)
    end
  end

  specify 'integration' do
    user = create :user, balance: 100_000

    expect(lottery).to receive :generate_result! do
      lottery.numbers = (1..8).to_a
      lottery.gun = 2
    end

    expect do
      10.times { lottery.participate! user, 'bet' => 30, 'numbers' => (1..8).to_a, 'guns' => [1] }
      100.times { lottery.participate! user, 'bet' => 30, 'numbers' => (9..16).to_a, 'guns' => [1] }

      lottery.process_end!
    end.to change { subject.reload.amount }.by (6600 - 200_000)
  end
end
