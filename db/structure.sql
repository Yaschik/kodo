--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: hstore; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hstore WITH SCHEMA public;


--
-- Name: EXTENSION hstore; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hstore IS 'data type for storing sets of (key, value) pairs';


SET search_path = public, pg_catalog;

--
-- Name: first_agg(anyelement, anyelement); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION first_agg(anyelement, anyelement) RETURNS anyelement
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
              SELECT $1;
      $_$;


--
-- Name: last_agg(anyelement, anyelement); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION last_agg(anyelement, anyelement) RETURNS anyelement
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
              SELECT $2;
      $_$;


--
-- Name: first(anyelement); Type: AGGREGATE; Schema: public; Owner: -
--

CREATE AGGREGATE first(anyelement) (
    SFUNC = first_agg,
    STYPE = anyelement
);


--
-- Name: last(anyelement); Type: AGGREGATE; Schema: public; Owner: -
--

CREATE AGGREGATE last(anyelement) (
    SFUNC = last_agg,
    STYPE = anyelement
);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: bot_orders; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bot_orders (
    id integer NOT NULL,
    role character varying,
    type character varying,
    data text,
    status text,
    error character varying,
    offer_link character varying,
    offer_id character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    run_at timestamp without time zone,
    hash_uid integer,
    user_id integer,
    priority integer,
    total_cost double precision
);


--
-- Name: bot_orders_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bot_orders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bot_orders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bot_orders_id_seq OWNED BY bot_orders.id;


--
-- Name: bots; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE bots (
    id integer NOT NULL,
    login_steam character varying NOT NULL,
    pass_steam character varying NOT NULL,
    login_mail character varying NOT NULL,
    pass_mail character varying NOT NULL,
    role character varying,
    code_link character varying,
    status character varying,
    error character varying
);


--
-- Name: bots_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE bots_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bots_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE bots_id_seq OWNED BY bots.id;


--
-- Name: creditings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE creditings (
    id integer NOT NULL,
    amount numeric(10,2),
    user_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    source character varying
);


--
-- Name: creditings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE creditings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: creditings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE creditings_id_seq OWNED BY creditings.id;


--
-- Name: fifties; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE fifties (
    id integer NOT NULL,
    user_id integer NOT NULL,
    current_step integer,
    factor integer NOT NULL,
    game_done boolean,
    win_amount integer[] DEFAULT '{0,0,0,0,0,0,0,0,0,0}'::integer[],
    history integer[] DEFAULT '{0,0,0,0,0,0,0,0,0}'::integer[],
    run_at timestamp without time zone
);


--
-- Name: fifties_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fifties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fifties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fifties_id_seq OWNED BY fifties.id;


--
-- Name: fifty_highs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE fifty_highs (
    id integer NOT NULL,
    high integer,
    end_time timestamp without time zone
);


--
-- Name: fifty_highs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fifty_highs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fifty_highs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fifty_highs_id_seq OWNED BY fifty_highs.id;


--
-- Name: headshot_lottery_stats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE headshot_lottery_stats (
    id integer NOT NULL,
    bets numeric(10,2) DEFAULT 0 NOT NULL,
    wins numeric(10,2) DEFAULT 0 NOT NULL,
    participations_count integer DEFAULT 0 NOT NULL,
    winners integer DEFAULT 0 NOT NULL,
    groups hstore DEFAULT ''::hstore NOT NULL,
    date date
);


--
-- Name: headshot_lottery_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE headshot_lottery_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: headshot_lottery_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE headshot_lottery_stats_id_seq OWNED BY headshot_lottery_stats.id;


--
-- Name: item_classes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE item_classes (
    id integer NOT NULL,
    name character varying NOT NULL,
    market_name character varying,
    market_hash_name character varying,
    steam_id character varying NOT NULL,
    icon character varying,
    icon_large character varying,
    price numeric(10,2),
    enabled boolean DEFAULT false NOT NULL,
    best boolean DEFAULT false NOT NULL,
    app_id integer,
    rareness character varying NOT NULL,
    weapon_type character varying NOT NULL,
    classfield character varying NOT NULL,
    stared boolean DEFAULT false NOT NULL,
    stattrak boolean DEFAULT false NOT NULL
);


--
-- Name: item_classes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE item_classes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: item_classes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE item_classes_id_seq OWNED BY item_classes.id;


--
-- Name: items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE items (
    id integer NOT NULL,
    item_class_id integer,
    steam_id character varying NOT NULL,
    bot_order_id integer
);


--
-- Name: items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE items_id_seq OWNED BY items.id;


--
-- Name: kodo_lottery_stats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE kodo_lottery_stats (
    id integer NOT NULL,
    participations_count integer DEFAULT 0 NOT NULL,
    winners integer DEFAULT 0 NOT NULL,
    lottery_id integer NOT NULL,
    bets numeric(10,2) DEFAULT 0 NOT NULL,
    wins numeric(10,2) DEFAULT 0 NOT NULL,
    date date NOT NULL,
    current_superprize numeric(10,2)
);


--
-- Name: kodo_lottery_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE kodo_lottery_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: kodo_lottery_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE kodo_lottery_stats_id_seq OWNED BY kodo_lottery_stats.id;


--
-- Name: login_numbers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE login_numbers (
    id integer NOT NULL,
    parametr character varying,
    updated_at timestamp without time zone DEFAULT '2016-01-15 19:04:09.365517'::timestamp without time zone NOT NULL
);


--
-- Name: login_numbers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE login_numbers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: login_numbers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE login_numbers_id_seq OWNED BY login_numbers.id;


--
-- Name: lotteries; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE lotteries (
    id integer NOT NULL,
    type character varying NOT NULL,
    time_from timestamp without time zone NOT NULL,
    time_till timestamp without time zone NOT NULL,
    result json,
    metadata json DEFAULT '{}'::json NOT NULL
);


--
-- Name: lotteries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE lotteries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lotteries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE lotteries_id_seq OWNED BY lotteries.id;


--
-- Name: lottery_lifetime_stats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE lottery_lifetime_stats (
    id integer NOT NULL,
    lottery_type character varying NOT NULL,
    data json NOT NULL
);


--
-- Name: lottery_lifetime_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE lottery_lifetime_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lottery_lifetime_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE lottery_lifetime_stats_id_seq OWNED BY lottery_lifetime_stats.id;


--
-- Name: lottery_metadata_number_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE lottery_metadata_number_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lottery_metadata_number_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE lottery_metadata_number_seq OWNED BY lotteries.metadata;


--
-- Name: lottery_participations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE lottery_participations (
    id integer NOT NULL,
    user_id integer,
    lottery_id integer,
    bet numeric(10,2),
    win numeric(10,2),
    selection json DEFAULT '{}'::json,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    win_metadata hstore DEFAULT ''::hstore NOT NULL,
    bet_metadata json DEFAULT '{}'::json NOT NULL
);


--
-- Name: lottery_participations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE lottery_participations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lottery_participations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE lottery_participations_id_seq OWNED BY lottery_participations.id;


--
-- Name: que_jobs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE que_jobs (
    priority smallint DEFAULT 100 NOT NULL,
    run_at timestamp with time zone DEFAULT now() NOT NULL,
    job_id bigint NOT NULL,
    job_class text NOT NULL,
    args json DEFAULT '[]'::json NOT NULL,
    error_count integer DEFAULT 0 NOT NULL,
    last_error text,
    queue text DEFAULT ''::text NOT NULL
);


--
-- Name: TABLE que_jobs; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE que_jobs IS '3';


--
-- Name: que_jobs_job_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE que_jobs_job_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: que_jobs_job_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE que_jobs_job_id_seq OWNED BY que_jobs.job_id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: stone_games; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE stone_games (
    id integer NOT NULL,
    status character varying,
    user1 integer,
    user2 integer,
    factor integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: stone_games_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stone_games_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stone_games_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stone_games_id_seq OWNED BY stone_games.id;


--
-- Name: stone_rounds; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE stone_rounds (
    id integer NOT NULL,
    stone_game integer,
    user1_pass character varying,
    user2_pass character varying,
    status character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: stone_rounds_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stone_rounds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stone_rounds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stone_rounds_id_seq OWNED BY stone_rounds.id;


--
-- Name: super_prizes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE super_prizes (
    id integer NOT NULL,
    lottery_type character varying NOT NULL,
    amount numeric(10,2) DEFAULT 0,
    ratio numeric(10,5) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: super_prizes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE super_prizes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: super_prizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE super_prizes_id_seq OWNED BY super_prizes.id;


--
-- Name: tickets; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tickets (
    id integer NOT NULL,
    user_id integer NOT NULL,
    category character varying DEFAULT 'issue'::character varying NOT NULL,
    title character varying,
    body text NOT NULL,
    closed boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_name character varying,
    user_email character varying
);


--
-- Name: tickets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tickets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: tickets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tickets_id_seq OWNED BY tickets.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    nickname character varying NOT NULL,
    steam_id character varying NOT NULL,
    role character varying DEFAULT 'guest'::character varying NOT NULL,
    balance numeric(10,2) DEFAULT 0,
    payments numeric(10,2) DEFAULT 0 NOT NULL,
    kodo_tickets integer DEFAULT 0 NOT NULL,
    headshot_tickets integer DEFAULT 0 NOT NULL,
    created_at timestamp without time zone DEFAULT '2016-01-15 19:04:07.876313'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone DEFAULT '2016-01-15 19:04:07.899621'::timestamp without time zone NOT NULL,
    steam_offer_link character varying,
    sound boolean DEFAULT true,
    avatar character varying DEFAULT 'http://api.adorable.io/avatars/95/abott@adorable.png'::character varying NOT NULL,
    CONSTRAINT balance_constraint CHECK ((balance >= (0)::numeric))
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bot_orders ALTER COLUMN id SET DEFAULT nextval('bot_orders_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY bots ALTER COLUMN id SET DEFAULT nextval('bots_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY creditings ALTER COLUMN id SET DEFAULT nextval('creditings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fifties ALTER COLUMN id SET DEFAULT nextval('fifties_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fifty_highs ALTER COLUMN id SET DEFAULT nextval('fifty_highs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY headshot_lottery_stats ALTER COLUMN id SET DEFAULT nextval('headshot_lottery_stats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY item_classes ALTER COLUMN id SET DEFAULT nextval('item_classes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY items ALTER COLUMN id SET DEFAULT nextval('items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY kodo_lottery_stats ALTER COLUMN id SET DEFAULT nextval('kodo_lottery_stats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY login_numbers ALTER COLUMN id SET DEFAULT nextval('login_numbers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY lotteries ALTER COLUMN id SET DEFAULT nextval('lotteries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY lottery_lifetime_stats ALTER COLUMN id SET DEFAULT nextval('lottery_lifetime_stats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY lottery_participations ALTER COLUMN id SET DEFAULT nextval('lottery_participations_id_seq'::regclass);


--
-- Name: job_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY que_jobs ALTER COLUMN job_id SET DEFAULT nextval('que_jobs_job_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stone_games ALTER COLUMN id SET DEFAULT nextval('stone_games_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stone_rounds ALTER COLUMN id SET DEFAULT nextval('stone_rounds_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY super_prizes ALTER COLUMN id SET DEFAULT nextval('super_prizes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tickets ALTER COLUMN id SET DEFAULT nextval('tickets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: bot_orders_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bot_orders
    ADD CONSTRAINT bot_orders_pkey PRIMARY KEY (id);


--
-- Name: bots_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY bots
    ADD CONSTRAINT bots_pkey PRIMARY KEY (id);


--
-- Name: creditings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY creditings
    ADD CONSTRAINT creditings_pkey PRIMARY KEY (id);


--
-- Name: fifties_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY fifties
    ADD CONSTRAINT fifties_pkey PRIMARY KEY (id);


--
-- Name: fifty_highs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY fifty_highs
    ADD CONSTRAINT fifty_highs_pkey PRIMARY KEY (id);


--
-- Name: headshot_lottery_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY headshot_lottery_stats
    ADD CONSTRAINT headshot_lottery_stats_pkey PRIMARY KEY (id);


--
-- Name: item_classes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY item_classes
    ADD CONSTRAINT item_classes_pkey PRIMARY KEY (id);


--
-- Name: items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY items
    ADD CONSTRAINT items_pkey PRIMARY KEY (id);


--
-- Name: kodo_lottery_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY kodo_lottery_stats
    ADD CONSTRAINT kodo_lottery_stats_pkey PRIMARY KEY (id);


--
-- Name: login_numbers_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY login_numbers
    ADD CONSTRAINT login_numbers_pkey PRIMARY KEY (id);


--
-- Name: lotteries_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY lotteries
    ADD CONSTRAINT lotteries_pkey PRIMARY KEY (id);


--
-- Name: lottery_lifetime_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY lottery_lifetime_stats
    ADD CONSTRAINT lottery_lifetime_stats_pkey PRIMARY KEY (id);


--
-- Name: lottery_participations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY lottery_participations
    ADD CONSTRAINT lottery_participations_pkey PRIMARY KEY (id);


--
-- Name: que_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY que_jobs
    ADD CONSTRAINT que_jobs_pkey PRIMARY KEY (queue, priority, run_at, job_id);


--
-- Name: stone_games_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stone_games
    ADD CONSTRAINT stone_games_pkey PRIMARY KEY (id);


--
-- Name: stone_rounds_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stone_rounds
    ADD CONSTRAINT stone_rounds_pkey PRIMARY KEY (id);


--
-- Name: super_prizes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY super_prizes
    ADD CONSTRAINT super_prizes_pkey PRIMARY KEY (id);


--
-- Name: tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_creditings_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_creditings_on_user_id ON creditings USING btree (user_id);


--
-- Name: index_fifties_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_fifties_on_user_id ON fifties USING btree (user_id);


--
-- Name: index_item_classes_on_best; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_item_classes_on_best ON item_classes USING btree (best);


--
-- Name: index_item_classes_on_steam_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_item_classes_on_steam_id ON item_classes USING btree (steam_id);


--
-- Name: index_items_on_bot_order_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_items_on_bot_order_id ON items USING btree (bot_order_id);


--
-- Name: index_items_on_item_class_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_items_on_item_class_id ON items USING btree (item_class_id);


--
-- Name: index_items_on_steam_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_items_on_steam_id ON items USING btree (steam_id);


--
-- Name: index_kodo_lottery_stats_on_lottery_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_kodo_lottery_stats_on_lottery_id ON kodo_lottery_stats USING btree (lottery_id);


--
-- Name: index_lottery_participations_on_lottery_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_lottery_participations_on_lottery_id ON lottery_participations USING btree (lottery_id);


--
-- Name: index_lottery_participations_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_lottery_participations_on_user_id ON lottery_participations USING btree (user_id);


--
-- Name: index_lottery_participations_on_user_id_and_lottery_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_lottery_participations_on_user_id_and_lottery_id ON lottery_participations USING btree (user_id, lottery_id);


--
-- Name: index_super_prizes_on_lottery_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_super_prizes_on_lottery_type ON super_prizes USING btree (lottery_type);


--
-- Name: index_tickets_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_tickets_on_user_id ON tickets USING btree (user_id);


--
-- Name: lottery_metadata_number_idx; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX lottery_metadata_number_idx ON lotteries USING btree ((((metadata ->> 'number'::text))::integer)) WHERE ((metadata -> 'number'::text) IS NOT NULL);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: fk_rails_1878c1a13f; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY kodo_lottery_stats
    ADD CONSTRAINT fk_rails_1878c1a13f FOREIGN KEY (lottery_id) REFERENCES lotteries(id);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20150417205645');

INSERT INTO schema_migrations (version) VALUES ('20150606082924');

INSERT INTO schema_migrations (version) VALUES ('20150607084325');

INSERT INTO schema_migrations (version) VALUES ('20150613103057');

INSERT INTO schema_migrations (version) VALUES ('20150623170951');

INSERT INTO schema_migrations (version) VALUES ('20150625175350');

INSERT INTO schema_migrations (version) VALUES ('20150628092929');

INSERT INTO schema_migrations (version) VALUES ('20150704110539');

INSERT INTO schema_migrations (version) VALUES ('20150704132558');

INSERT INTO schema_migrations (version) VALUES ('20150704153513');

INSERT INTO schema_migrations (version) VALUES ('20150704224302');

INSERT INTO schema_migrations (version) VALUES ('20150705081522');

INSERT INTO schema_migrations (version) VALUES ('20150705103314');

INSERT INTO schema_migrations (version) VALUES ('20150705110303');

INSERT INTO schema_migrations (version) VALUES ('20150707172256');

INSERT INTO schema_migrations (version) VALUES ('20150713120517');

INSERT INTO schema_migrations (version) VALUES ('20150713131421');

INSERT INTO schema_migrations (version) VALUES ('20150714125346');

INSERT INTO schema_migrations (version) VALUES ('20150715175402');

INSERT INTO schema_migrations (version) VALUES ('20150716142341');

INSERT INTO schema_migrations (version) VALUES ('20150716150047');

INSERT INTO schema_migrations (version) VALUES ('20150723115950');

INSERT INTO schema_migrations (version) VALUES ('20150723122840');

INSERT INTO schema_migrations (version) VALUES ('20150725133921');

INSERT INTO schema_migrations (version) VALUES ('20150725182028');

INSERT INTO schema_migrations (version) VALUES ('20150725221722');

INSERT INTO schema_migrations (version) VALUES ('20150728164954');

INSERT INTO schema_migrations (version) VALUES ('20150808194209');

INSERT INTO schema_migrations (version) VALUES ('20150808200051');

INSERT INTO schema_migrations (version) VALUES ('20150809133222');

INSERT INTO schema_migrations (version) VALUES ('20150816165650');

INSERT INTO schema_migrations (version) VALUES ('20150902181652');

INSERT INTO schema_migrations (version) VALUES ('20150902182317');

INSERT INTO schema_migrations (version) VALUES ('20150902184541');

INSERT INTO schema_migrations (version) VALUES ('20151010144223');

INSERT INTO schema_migrations (version) VALUES ('20151015224808');

INSERT INTO schema_migrations (version) VALUES ('20151019113138');

INSERT INTO schema_migrations (version) VALUES ('20151027195933');

INSERT INTO schema_migrations (version) VALUES ('20151030154347');

INSERT INTO schema_migrations (version) VALUES ('20151111134054');

INSERT INTO schema_migrations (version) VALUES ('20151111134304');

INSERT INTO schema_migrations (version) VALUES ('20151117141735');

INSERT INTO schema_migrations (version) VALUES ('20151126212652');

INSERT INTO schema_migrations (version) VALUES ('20151202152139');

INSERT INTO schema_migrations (version) VALUES ('20151202211433');

INSERT INTO schema_migrations (version) VALUES ('20151202211848');

INSERT INTO schema_migrations (version) VALUES ('20151214010039');

INSERT INTO schema_migrations (version) VALUES ('20151229135244');

INSERT INTO schema_migrations (version) VALUES ('20151229135653');

INSERT INTO schema_migrations (version) VALUES ('20151229174147');

INSERT INTO schema_migrations (version) VALUES ('20160107134743');

