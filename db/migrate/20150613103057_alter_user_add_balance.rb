class AlterUserAddBalance < ActiveRecord::Migration
  def change
    add_column :users, :balance, :decimal, scale: 2, precision: 10, default: 0

    reversible do |d|
      d.up do
        execute <<-SQL
          ALTER TABLE users
            ADD CONSTRAINT balance_constraint
              CHECK(balance >= 0)
        SQL
      end

      d.down do
        execute <<-SQL
          ALTER TABLE users
            DROP CONSTRAINT balance_constraint
        SQL
      end
    end
  end
end
