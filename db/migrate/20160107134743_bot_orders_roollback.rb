class BotOrdersRoollback < ActiveRecord::Migration
  def change
    add_column :bot_orders, :user_id, :integer
    add_column :bot_orders, :priority, :integer
    add_column :bot_orders, :total_cost, :float
  end
end
