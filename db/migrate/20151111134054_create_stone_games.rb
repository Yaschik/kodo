class CreateStoneGames < ActiveRecord::Migration
  def change
    create_table :stone_games do |t|
      t.string :status
      t.integer :user1
      t.integer :user2
      t.integer :factor
      t.timestamps :open_at
      t.timestamps :close_at
    end
  end
end
