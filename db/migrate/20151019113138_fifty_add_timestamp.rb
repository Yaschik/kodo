class FiftyAddTimestamp < ActiveRecord::Migration
  Fifty = Class.new(ActiveRecord::Base)
  def up
    add_column :fifties, :run_at, :timestamp

  end
  def down
    remove_column :fifties, :run_at
  end
end
