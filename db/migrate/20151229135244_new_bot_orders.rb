class NewBotOrders < ActiveRecord::Migration
  def change
    remove_column :bot_orders, :user_id
    remove_column :bot_orders, :status
    remove_column :bot_orders, :created_at
    remove_column :bot_orders, :updated_at
    remove_column :bot_orders, :type
    remove_column :bot_orders, :priority
    remove_column :bot_orders, :error
    remove_column :bot_orders, :offer_id
    remove_column :bot_orders, :run_at
    remove_column :bot_orders, :total_cost
  end
end
