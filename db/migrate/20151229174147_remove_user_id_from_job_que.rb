class RemoveUserIdFromJobQue < ActiveRecord::Migration
  def change
    remove_column :que_jobs, :user_id
  end
end
