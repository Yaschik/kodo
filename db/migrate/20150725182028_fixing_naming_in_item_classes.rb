class FixingNamingInItemClasses < ActiveRecord::Migration
  def change
    remove_column :item_classes, :quantity
  end
end
