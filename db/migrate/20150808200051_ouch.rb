class Ouch < ActiveRecord::Migration
  def change
    rename_column :item_classes, :stattrack, :stattrak
  end
end
