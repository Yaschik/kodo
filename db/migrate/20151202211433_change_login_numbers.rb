class ChangeLoginNumbers < ActiveRecord::Migration
  def change
    add_column :login_numbers, :updated_at, :timestamp, null: false, default: Time.zone.now
  end
end
