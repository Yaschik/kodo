class AlterUsersAddSteamOfferId < ActiveRecord::Migration
  def change
    add_column :users, :steam_offer_id, :string, null: true
  end
end
