class CreateStoneRounds < ActiveRecord::Migration
  def change
    create_table :stone_rounds do |t|
      t.integer :stone_game
      t.string :user1_pass
      t.string :user2_pass
      t.string :status
      t.timestamps :start_at
    end
  end
end
