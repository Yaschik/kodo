class CreateLotteriesAndParticipations < ActiveRecord::Migration
  def change
    create_table :lotteries do |t|
      t.string :type, null: false

      t.timestamp :time_from, null: false
      t.timestamp :time_till, null: false

      t.json :result, null: true
    end

    create_table :lottery_participations do |t|
      t.references :user, index: true
      t.references :lottery, index: true

      t.decimal :bet, scale: 2, precision: 10, null: true
      t.decimal :win, scale: 2, precision: 10, null: true

      t.json :selection

      t.timestamps null: false
    end

    add_index :lottery_participations, %i[user_id lottery_id]
  end
end
