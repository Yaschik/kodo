class CollectCurrentJackpotInKodoStats < ActiveRecord::Migration
  def change
    add_column :kodo_lottery_stats, :current_superprize,
               :decimal, scale: 2, precision: 10, null: true

    Lottery::Kodo::Stat.find_each do |stat|
      stat.update! current_superprize: stat.lottery.current_superprize
    end

    change_column_null :kodo_lottery_stats, :current_superprize, false
  end
end
