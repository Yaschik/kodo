class AlterLotteriesAddMetadata < ActiveRecord::Migration
  def change
    add_column :lotteries, :metadata, :json, null: false, default: {}
  end
end
