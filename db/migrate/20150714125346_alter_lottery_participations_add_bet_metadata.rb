class AlterLotteryParticipationsAddBetMetadata < ActiveRecord::Migration
  def change
    add_column :lottery_participations, :bet_metadata, :json, null: false, default: {}
  end
end
