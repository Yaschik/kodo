class UpdateSchemaToBotsSchema < ActiveRecord::Migration
  BotOrder = Class.new(ActiveRecord::Base)
  ItemClass = Class.new(ActiveRecord::Base)
  def change
    reversible do |d|
      d.up do
        BotOrder.delete_all
        ItemClass.delete_all
      end
    end

    change_table :bot_orders do |t|
      t.string :type, null: false
      t.integer :priority, null: false, default: 100
      t.text :error, null: true
      t.string :offer_id, null: true
      t.timestamp :run_at, null: false, default: 'epoch'
    end

    change_table :item_classes do |t|
      t.string :rareness, null: false
      t.string :weapon_type, null: false
      t.string :classfield, null: false
      t.boolean :stared, null: false, default: false
      t.boolean :startrack, null: false, default: false
    end

    reversible do |d|
      d.up do
        change_column :item_classes, :steam_id, :string, null: false
        change_column :items, :steam_id, :string, null: false
      end

      d.down do
        change_column :item_classes, :steam_id, :integer, null: false
        change_column :items, :steam_id, :integer, null: false
      end
    end
  end
end
