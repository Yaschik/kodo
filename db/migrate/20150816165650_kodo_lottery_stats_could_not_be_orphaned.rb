class KodoLotteryStatsCouldNotBeOrphaned < ActiveRecord::Migration
  def up
    Lottery::Kodo::Stat.where(lottery: nil).delete_all
    change_column_null :kodo_lottery_stats, :lottery_id, false
    delete_orphans!
    add_foreign_key :kodo_lottery_stats, :lotteries
  end

  def delete_orphans!
    orphan_ids = Lottery::Kodo::Stat
                 .joins('LEFT JOIN lotteries ON lotteries.id = lottery_id')
                 .select('kodo_lottery_stats.id')
    Lottery::Kodo::Stat.where(id: orphan_ids).delete_all
  end

  def down
    change_column_null :kodo_lottery_stats, :lottery_id, true
    remove_foreign_key :kodo_lottery_stats, :lotteries
  end
end
