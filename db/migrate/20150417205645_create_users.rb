class CreateUsers < ActiveRecord::Migration
  def up
    create_table :users do |t|
      t.string :nickname, null: false
      t.string :steam_id, null: false, unique: true
      t.string :role, null: false, default: 'guest'
    end
  end

  def down
    drop_table :users
  end
end
