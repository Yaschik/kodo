class RenameStartrack < ActiveRecord::Migration
  def change
    rename_column :item_classes, :startrack, :stattrack
  end
end
