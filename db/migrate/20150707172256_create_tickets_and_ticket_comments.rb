class CreateTicketsAndTicketComments < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.references :user, null: false, index: true
      t.string :category, null: false, default: 'issue'
      t.string :title
      t.text :body, null: false
      t.boolean :closed, default: false, null: false
      t.timestamps null: false
    end

    create_table :ticket_comments do |t|
      t.references :ticket, null: false, index: true
      t.references :user, null: false, index: true
      t.text :body, null: false
      t.timestamps null: false
    end
  end
end
