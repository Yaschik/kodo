class AddTotalCostToBotOrderId < ActiveRecord::Migration
  def up
    add_column :bot_orders, :total_cost, :decimal, scale: 2, precision: 10, null: false, default: 0
    change_column :bot_orders, :total_cost, :decimal, scale: 2, precision: 10, null: false
    change_column :bot_orders, :user_id, :integer, null: true
  end

  def down
    remove_column :bot_orders, :total_cost
    change_column :bot_orders, :user_id, :integer, null: false
  end
end
