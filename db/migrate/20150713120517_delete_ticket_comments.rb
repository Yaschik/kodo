class DeleteTicketComments < ActiveRecord::Migration
  def up
    drop_table :ticket_comments
  end

  def down
    create_table :ticket_comments do |t|
      t.references :ticket, null: false, index: true
      t.references :user, null: false, index: true
      t.text :body, null: false
      t.timestamps null: false
    end
  end
end
