class ChangeKodoLifetimeStat < ActiveRecord::Migration
  LotteryLifetimeStat = Class.new(ActiveRecord::Base)

  def up
    LotteryLifetimeStat.where(lottery_type: 'Lottery::Kodo').delete_all
    LotteryLifetimeStat.create!(
      lottery_type: 'Lottery::Kodo',
      data: {
        numbers: Lottery::Kodo.finished.numbers_stats.map do |x|
          [x.fetch(:number), count: x.fetch(:count), last_seen: -1]
        end.to_h,
      },
    )
  end

  def down
    LotteryLifetimeStat.where(lottery_type: 'Lottery::Kodo').delete_all
  end
end
