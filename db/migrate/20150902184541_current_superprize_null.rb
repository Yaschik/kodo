class CurrentSuperprizeNull < ActiveRecord::Migration
  def change
    change_column_null :kodo_lottery_stats, :current_superprize, true
  end
end
