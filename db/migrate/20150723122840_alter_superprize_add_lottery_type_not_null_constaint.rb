class AlterSuperprizeAddLotteryTypeNotNullConstaint < ActiveRecord::Migration
  def change
    change_column :super_prizes, :lottery_type, :string, null: false
  end
end
