class AddSoundToUsers < ActiveRecord::Migration
  def change
    add_column :users, :sound, :boolean, null: true, default: true
  end
end
