class CreateNewOrders < ActiveRecord::Migration
  def change
    add_column :bot_orders, :role, :string
    add_column :bot_orders, :type, :string
    add_column :bot_orders, :data, :text
    add_column :bot_orders, :status, :text
    add_column :bot_orders, :error, :string
    add_column :bot_orders, :offer_link, :string
    add_column :bot_orders, :offer_id, :string
    add_column :bot_orders, :created_at, :timestamp
    add_column :bot_orders, :updated_at, :timestamp
    add_column :bot_orders, :run_at, :timestamp
    add_column :bot_orders, :hash_uid, :integer
  end
end
