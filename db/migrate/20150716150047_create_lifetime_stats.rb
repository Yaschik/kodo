class CreateLifetimeStats < ActiveRecord::Migration
  LotteryLifetimeStat = Class.new(ActiveRecord::Base)

  def load_lottery_lifetime_stats!
    LotteryLifetimeStat.create!(
      lottery_type: 'Lottery::Kodo',
      data: {
        numbers: Lottery::Kodo.finished.numbers_stats,
      },
    )
  end

  def change
    create_table :lottery_lifetime_stats do |t|
      t.string :lottery_type, null: false
      t.json :data, null: false
    end

    reversible do |dir|
      dir.up { load_lottery_lifetime_stats! }
    end
  end
end
