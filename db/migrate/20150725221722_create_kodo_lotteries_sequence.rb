class CreateKodoLotteriesSequence < ActiveRecord::Migration
  def up
    connection.execute <<-SQL
      CREATE INDEX lottery_metadata_number_idx
      ON lotteries(((metadata ->> 'number')::integer))
      WHERE (metadata -> 'number') IS NOT NULL;

      CREATE SEQUENCE lottery_metadata_number_seq OWNED BY lotteries.metadata;
    SQL
  end

  def down
    connection.execute <<-SQL
      DROP INDEX lottery_metadata_number_idx;
      DROP SEQUENCE lottery_metadata_number_seq;
    SQL
  end
end
