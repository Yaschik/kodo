item_classes_data = JSON.parse <<-JSON
  {"520026215": {"rareness": "\u0417\u0430\u043a\u0430\u043b\u0435\u043d\u043d\u043e\u0435 \u0432 \u0431\u043e\u044f\u0445", "name": "Galil AR | \u041e\u0445\u043e\u0442\u043d\u0438\u0447\u044c\u044f \u0431\u0443\u0434\u043a\u0430", "market_name": "Galil AR | \u041e\u0445\u043e\u0442\u043d\u0438\u0447\u044c\u044f \u0431\u0443\u0434\u043a\u0430 (\u0417\u0430\u043a\u0430\u043b\u0435\u043d\u043d\u043e\u0435 \u0432 \u0431\u043e\u044f\u0445)", "app_id": "730", "weapon_type": "\u0412\u0438\u043d\u0442\u043e\u0432\u043a\u0430", "classfield": "\u0428\u0438\u0440\u043f\u043e\u0442\u0440\u0435\u0431", "market_hash_name": "Galil AR | Hunting Blind (Battle-Scarred)", "icon_large": "-9a81dlWLwJ2UUGcVs_nsVtzdOEdtWwKGZZLQHTxDZ7I56KU0Zwwo4NUX4oFJZEHLbXH5ApeO4YmlhxYQknCRvCo04DEVlxkKgposbupIgthwczbYQJP6c--q5OHluT8Nqjunm5Q_txOhujT8om7i1Hg_0I_YGDyIdKWJ1RtNw3R8lK7lL--1MPq757OzSEx73Vxt3iOnAv3309KxEWqOg", "icon": "-9a81dlWLwJ2UUGcVs_nsVtzdOEdtWwKGZZLQHTxDZ7I56KU0Zwwo4NUX4oFJZEHLbXH5ApeO4YmlhxYQknCRvCo04DEVlxkKgposbupIgthwczbYQJP6c--q5OHluT8Nqjunm5Q_tw_ieyVod322w3l_hdvNjugctOUegU7NAmBqwToku3ogcXu6ciam3UwuT5iuyio602etQ"}, "310778216": {"rareness": "\u0417\u0430\u043a\u0430\u043b\u0435\u043d\u043d\u043e\u0435 \u0432 \u0431\u043e\u044f\u0445", "name": "Galil AR | \u041e\u0445\u043e\u0442\u043d\u0438\u0447\u044c\u044f \u0431\u0443\u0434\u043a\u0430", "market_name": "Galil AR | \u041e\u0445\u043e\u0442\u043d\u0438\u0447\u044c\u044f \u0431\u0443\u0434\u043a\u0430 (\u0417\u0430\u043a\u0430\u043b\u0435\u043d\u043d\u043e\u0435 \u0432 \u0431\u043e\u044f\u0445)", "app_id": "730", "weapon_type": "\u0412\u0438\u043d\u0442\u043e\u0432\u043a\u0430", "classfield": "\u0428\u0438\u0440\u043f\u043e\u0442\u0440\u0435\u0431", "market_hash_name": "Galil AR | Hunting Blind (Battle-Scarred)", "icon_large": "-9a81dlWLwJ2UUGcVs_nsVtzdOEdtWwKGZZLQHTxDZ7I56KU0Zwwo4NUX4oFJZEHLbXH5ApeO4YmlhxYQknCRvCo04DEVlxkKgposbupIgthwczbYQJP6c--q5OHluT8Nqjunm5Q_txOhujT8om7i1Hg_0I_YGDyIdKWJ1RtNw3R8lK7lL--1MPq757OzSEx73Vxt3iOnAv3309KxEWqOg", "icon": "-9a81dlWLwJ2UUGcVs_nsVtzdOEdtWwKGZZLQHTxDZ7I56KU0Zwwo4NUX4oFJZEHLbXH5ApeO4YmlhxYQknCRvCo04DEVlxkKgposbupIgthwczbYQJP6c--q5OHluT8Nqjunm5Q_tw_ieyVod322w3l_hdvNjugctOUegU7NAmBqwToku3ogcXu6ciam3UwuT5iuyio602etQ"}}
JSON

ActiveRecord::Base.transaction do
  yaml = YAML.load(ERB.new(File.read(Rails.root.join('db', 'seeds.yml'))).result)
  yaml.each do |model, data|
    klass = model.singularize.camelize.constantize
    klass.delete_all
    klass.create!(data)
    Rails.logger.info "Loaded seeds for model #{klass.name}"
  end

  ItemClass.delete_all
  Item.delete_all
  item_classes_data.each do |key, value|
    ic = ItemClass.create! value.merge(steam_id: key)
    ic.items.create! steam_id: (ic.steam_id + '1')
  end
  ItemClassCache.reload!
end
