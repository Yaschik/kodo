class Tainbox::ArrayOfHandler
  attr_reader :nested_klass

  def to_s
    'array_of_converter'
  end

  def initialize(klass)
    @nested_klass = klass
  end
end

module Tainbox::ClassMethods
  def array_of(klass)
    Tainbox::ArrayOfHandler.new(klass)
  end
end

Tainbox.define_converter(Tainbox::ArrayOfHandler.new(nil)) do
  array = value.split(/[\,\s]/).select(&:present?) if value.is_a? String
  array ||= value

  array.map do |x|
    Tainbox::TypeConverter.new(type.nested_klass, x, options: options).convert
  end
end

Tainbox.define_converter(Class) do
  return unless value
  return value if value.is_a?(Class)
  class_name = options[:prefix] ? options[:prefix] + value : value
  class_name.camelize.constantize
end
