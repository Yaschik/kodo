class Hash
  def pretty_fetch(*keys)
    if keys.first.is_a? Array
      raise ArgumentError unless keys.length == 1
      keys = keys.first
    end

    keys.each do |key|
      value = self[key]
      return value if value.present?
    end

    values.find(&:present?)
  end
end
