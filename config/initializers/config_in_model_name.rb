module ActiveModel::NamePrepender
  attr_reader :config_path

  def initialize(*)
    super
    @config_path = Rails.root.join 'config/models', i18n_key.to_s
  end
end

ActiveModel::Name.prepend ActiveModel::NamePrepender
