Rails.application.routes.draw do

  root 'home#index'

  namespace :admin do
    get '/', to: redirect('/admin/stats')

    resources :item_classes, only: :index do
      collection do
        patch :'/', as: :update_all, action: :update_all
        post :reload
      end
    end

    resources :tickets, only: %i[index show] do
      member do
        delete :close
        post :open
      end
    end

    resources :stats do
      member do
        delete :close
        post :open
      end
    end

    namespace :lotteries do
      resources :kodo, only: %i[show index] do
        collection do
          get '/by_date/:date', action: :by_date, as: :by_date
        end
      end

      resources :stone, only: %i[index] do
        collection do
          get '/close/:id', action: :close, as: :close
          get '/show/:id', action: :show, as: :show
        end
      end

      resources :headshot, only: %i[index] do
          collection do
          get '/by_date/:date', action: :by_date, as: :by_date
        end
      end
    end

    resources :users, only: %i[index show update] do
      member do
        get :participations
        get :items

        resources :creditings, only: %i[index create]
      end
    end

    resources :tickets, only: %i[index show] do
      member do
        delete :close
        post :open
      end
    end
  end

  post '/auth/steam/callback' => 'sessions#create'

  resource :session, only: %i[show update destroy]

  resource :fifty_high, only: %i[show]

  resources :tickets, only: %i[new create]

  resource :login_number, only: %i[show create]

  resource :fifties, only: %i[show create] do
    post :bet
  end

  resource :stones, only: %i[show create] do
    post :participate
    post :bet
    get :search
    get :get_info
    get :users
    get :winner
    get :history
    delete :leave
  end

  resource :stats, only: %i[show]

  resources :lotteries, only: %i[show] do
    member do
      post :bet
      get :show
      get :last
      post :multi_bet
      get :wait

      resources :participations, only: %i[index], controller: :lottery_participations do
        collection do
          get :today
          get :stats
        end
      end
    end
  end

  resources :items, only: %i[index] do
    collection do
      post :buy
    end
  end
  
  scope :filter, controller: 'filtered_items' do
    get :getfilter
    get :showpage
  end

  scope :robokassa, controller: 'robokassa' do
    post :result
    post :success
    post :fail
    get :gotourl
    post :signer
  end
end
