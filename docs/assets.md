
###Directives & [Controllers]
####lottery[Ctrl]
Функционал лоттерей kodo.
####callback[Ctrl]
Обратная связь: валидация и отправка файлов
####archive[Ctrl]
Выгрузка и фильтрация данных арихива лоттереи
####history[Ctrl]
Выгрузка и фильтрация ставок пользователя
####store[Ctrl]
Выгрузка товаров, корзина и покупка
####headshot[Ctrl]
Функционал headshot
####stat[Ctrl]
Вывод статистики
####broadcast[Ctrl]
Трансляция лоттереи kodo
####bets[Ctrl]
Попап со ставками пользователя
####error
Ошибка 404

###Gulp command
####sprite
Создвает спрайт из изображений с префиксом i- из всех папок.
Созданный спрайт и стили копирует в папку i-icons
####dev
Команда для разработки с открытием в firefox и explorer
####watch
Команда для разработки без открытием в firefox и explorer
####build
Команда для сборки
+ copy minify images
+ make and minify css
+ make and minify js
####ftp
Команда для загрузки файлов по ftp
Конфигурационный файл fpt.json в формате
```json
{
	"host": "alanev.ru",
	"user": "alanev_user",
	"pass": "***********"
}
```
####zip
Создает два архива (с собранными файлами и с разрабатываевыми) и отправляет на серевер по ftp с настройками из ftp.json

###Postcss
####css syntax
+ css variables https://github.com/MadLittleMods/postcss-css-variables
+ media variables https://github.com/postcss/postcss-custom-media
+ color funcion https://github.com/postcss/postcss-color-function
+ not selector https://github.com/postcss/postcss-selector-not
####css shortcuts
+ nested https://github.com/postcss/postcss-nested
+ short https://github.com/jonathantneal/postcss-short
+ focus https://github.com/postcss/postcss-focus
####css optimization